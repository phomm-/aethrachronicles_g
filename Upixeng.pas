unit Upixeng;

interface

uses Windows, Graphics, controls, classes, SysUtils, Types,

  // Self-made units
  UCommon, UGraph, UResMan, UMover;

type

  TGameImgObjColl = class; // forward declarations
  TGameIntfPart = class;
  TGameIntfLstColl = class;
  TGameIntfPartsColl = class;
  TGameIntfLst = class;
  TGameIntfLstItem = class;
  TGamePixels = class;
  TGameIntfField = class;
  //------------------------------------------------------------------------------

  Maskar = array of array of Boolean;

  TMask = class(TObject)
  private
    FCells: Maskar;
    FMskHeight: Word;
    FMskWidth: Word;
  public
    procedure Reset(AHeight, AWidth: Word; Val: Boolean = False);
    property MskWidth: Word read FMskWidth;
    property MskHeight: Word read FMskHeight;
    property Cells: Maskar read FCells;
    destructor Destroy; override;
  end;

  //------------------------------------------------------------------------------

  TGameImgObj = class(TCollectionItem) // ancestor for all Visible Game Objects
  private
    {FXCord, FYCord,}FGioWidth, FGioHeight: Word; // maybe Self or Ownr defined
    FMask: TMask;
    FChanged: Boolean;
    FOnTick: TNotifyEvent;
    FMover: TMover;
    FLockCnt: Integer;
    function GetImgColl(): TGameImgObjColl;
    procedure SetgioHeight(const Value: Word); virtual;
    procedure SetgioWidth(const Value: Word); virtual;
    procedure SetXCord(const Value: Word);
    procedure SetYCord(const Value: Word);
    procedure SetOnTick(const Value: TNotifyEvent);
    procedure SetMover(const Value: TMover);

  protected
    FMouse: TMouseEvent;
    FOwnr: TGameIntfPart;
    procedure SetGraphic(const Value: TGraphic); virtual; abstract;
    function GetGraphic(): TGraphic; virtual; abstract;
    property wXCord: Word write SetXCord;
    property wYCord: Word write SetYCord;
    property wgioWidth: Word write SetgioWidth;
    property wgioHeight: Word write SetgioHeight;
    procedure MouseUp(Shift: tShiftState); virtual;
    procedure MouseDown(Shift: tShiftState); virtual;
    procedure Mouseenter(Shift: tShiftState); virtual;
    procedure Mouseleave(Shift: tShiftState); virtual;
    procedure MouseMove(ay, ax: Word; Shift: tShiftState); virtual;
    procedure SetColl(aCollection: TGameImgObjColl);
    procedure SetMask(AMask: TMask);
    procedure SetOwnr(const Value: TGameIntfPart);
    function GetOwnr: TGameIntfPart;
    function GetgioHeight: Word; virtual;
    function GetgioWidth: Word; virtual;
    function GetXCord: Word; virtual;
    function GetYCord: Word; virtual;
    function ClientX(ax: Word): Word;
    function ClientY(ay: Word): Word;
    procedure SetChanged(const Value: Boolean); virtual;
    procedure Tick; virtual;

  public

    DrawDisabled,
      ActDisabled: Boolean;
    //Ownr : TGameIntfPart; // should be the ancestor of all GameObj-s
    //Ownrcls : tclass;
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    function NeedDraw: Boolean;
    procedure Lock;
    procedure UnLock;
    property Ownr: TGameIntfPart read GetOwnr write SetOwnr;
    property Graphic: TGraphic read GetGraphic write SetGraphic;
    property ImgColl: TGameImgObjColl read GetImgColl write SetColl;
    property Mask: TMask read fMask write SetMask;
    property xCord: Word read GetXCord;
    property yCord: Word read GetYCord;
    property gioWidth: Word read GetgioWidth;
    property gioHeight: Word read GetgioHeight;
    property onMouse: TMouseEvent read fMouse write fMouse;
    property IsChanged: Boolean read FChanged write SetChanged;
    function Gamepix: TGamePixels;
    property OnTick: TNotifyEvent read FOnTick write SetOnTick;
    property Mover: TMover read FMover write SetMover;
  end;

  //------------------------------------------------------------------------------

  TGameImgIntf = class(TGameImgObj)
  private
    fBmp: TBmp;
  protected
    function GetGraphic(): TGraphic; override;
    procedure SetGraphic(const Value: TGraphic); override;
    procedure MouseUp(Shift: tShiftState); override;
    procedure MouseDown(Shift: tShiftState); override;
    procedure Mouseenter(Shift: tShiftState); override;
    procedure Mouseleave(Shift: tShiftState); override;
  public
    destructor Destroy(); override;
    constructor Create(Collection: TCollection); override;
    procedure SetBmp(aBmp: TBmp; aTransCol: Cardinal = clwhite);
    property Graphic: TGraphic read GetGraphic write SetGraphic;
  end;

  TGameImgAnim = class(TGameImgIntf)
  private
    FImgLst: TImageList;
    Fcadre: Integer;
    TmpBmp: Tbmp;
  protected
    function GetGraphic(): TGraphic; override;
    function GetgioHeight: Word; override;
    function GetgioWidth: Word; override;
    procedure SetGraphic(const Value: TGraphic); override;
    procedure Tick; override;
    procedure mouseup(Shift: tShiftState); override;
  public
    property ImgLst: TImageList read FImgLst write FImgLst;
    property Graphic: TGraphic read GetGraphic write SetGraphic;
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;

  end;

  NJobMode = (jmNone, jmHorz, jmVert, jmBoth);

  TGameImgBkgd = class(TGameImgIntf)
  private
    FbmpIn: TBmp;
    FJobMode: NJobMode;
  protected
    function GetGraphic(): TGraphic; override;
  public
    destructor Destroy(); override;
    constructor Create(Collection: TCollection); override;
    property JobMode: NJobMode read FJobMode write FJobMode;
  end;

  TGameImgPavedBkgd = class(TGameImgBkgd)
  protected
    function GetGraphic(): TGraphic; override;
  end;

  TGameImgFlipedBkgd = class(TGameImgBkgd)
  protected
    function GetGraphic(): TGraphic; override;
  end;

  TGameImgStretchedBkgd = class(TGameImgBkgd)
  protected
    function GetGraphic(): TGraphic; override;
  end;
  //------------------------------------------------------------------------------

  TGameImgLst = class(TGameImgIntf)
  protected
    procedure SetOwnr(const Value: TGameIntfLst);
    function GetOwnr: TGameIntfLst;
    function GetGraphic: TGraphic; override;
  public
    property Ownr: TGameIntfLst read GetOwnr write SetOwnr;

  end;

  //------------------------------------------------------------------------------

  TGameImgLstItem = class(TGameImgIntf)
  protected
    function GetGraphic: TGraphic; override;
    procedure SetOwnr(const Value: TGameIntfLstItem);
    function GetOwnr: TGameIntfLstItem;
    function GetgioHeight: Word; override;
    function GetgioWidth: Word; override;
    function GetXCord: Word; override;
    function GetYCord: Word; override;
    procedure SetChanged(const Value: Boolean); override;
  public
    property Ownr: TGameIntfLstItem read GetOwnr write SetOwnr;
    procedure SetBmp(aBmp: TBmp);
    function Coll: TGameImgObjColl;
  end;

  //------------------------------------------------------------------------------

  TGameImgmap = class(TGameImgIntf)
  private
    CellIdent, ScrollIdent: Byte;
    FXShift, FYShift: SmallInt;
    //FOwnr : TGameIntfPart;
    FMapBuffer, // the Buffer of tiled Part of map, is diVisible by CellSize
    FMapDrawBuf: TBmp;
    FBufRect: TRect;

    function GetViewZone: TRect; // the Buffer to draw to GamePixels
    procedure SetgioHeight(const Value: Word); override;
    procedure SetgioWidth(const Value: Word); override;
    procedure SetBufRect();

    //procedure SetxShift(const  Value: SmallInt);
    //procedure SetyShift(const  Value: SmallInt);
    function GetOwnr(): TGameIntfField;
    procedure SetOwnr(const Value: TGameIntfField);
  protected
    function GetGraphic: TGraphic; override;
    procedure DrawCell(ay, ax: TCellInt);
    procedure PointToCell(ay, ax: Word; var aRow, aCol: TCellint);
    procedure RemapBuf(adir: NDiRection);
    procedure Tick; override;
  public
    destructor Destroy(); override;
    constructor Create(Collection: TCollection); override;
    procedure SetView(ay, ax: TCellInt);
    procedure MouseUp(Shift: tShiftState); override;
    procedure MouseDown(Shift: tShiftState); override;
    procedure Mouseenter(Shift: tShiftState); override;
    procedure Mouseleave(Shift: tShiftState); override;
    procedure MouseMove(ay, ax: Word; Shift: tShiftState); override;
    procedure Scroll(adir: NDiRection);
    property Ownr: TGameIntfField read GetOwnr write SetOwnr;
    property xShift: SmallInt read FxShift;
    property yShift: SmallInt read FyShift;
    property ViewZone: TRect read GetViewZone;
    property BufRect: TRect read FBufRect;

  end;

  //------------------------------------------------------------------------------

  TGameImgObjColl = class(TCollection)
  private
    fGamePix: TGamePixels;
    function GetItem(Index: Integer): TGameImgObj;
    procedure SetItem(Index: Integer; const Value: TGameImgObj);

  public
    procedure Tick;
    function Add(): TGameImgObj;
    procedure activate(B: Boolean);
    procedure dellast;
    constructor Create(aGamePix: TGamePixels);
    property Items[Index: Integer]: TGameImgObj read GetItem write SetItem;
    default;
    property GamePix: TGamePixels read fGamePix;
  end;

  //------------------------------------------------------------------------------
  // ancestor for all interface Game Objects
  TGameIntfPart = class(TCollectionItem, IKeyUsable)

    // IKeyUsable part
  private
    Fkey: Word;
    Fshift: TShiftState;
    FEvent: TNotifyEvent;
  protected
    FRefCount: Integer;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    property RefCount: Integer read FRefCount;
    procedure KeyPress(Key: Word; Shift: TShiftState);
    procedure RegKey(Key: Word; Shift: TShiftState; Event: TNotifyEvent);
    // IKeyUsable part

  private
    fxCord, fyCord, fgipWidth, fgipHeight: Word;
  protected
    FImgObj: TGameImgIntf;
    procedure SetgipHeight(const Value: Word); virtual;
    procedure SetgipWidth(const Value: Word); virtual;
    procedure SetxCord(const Value: Word);
    procedure SetyCord(const Value: Word);
    function GetxCord: Word; virtual;
    function GetyCord: Word; virtual;

    function GetImgObj(): TGameImgIntf;
    procedure SetImgObj(const Value: TGameImgIntf);
  public

    //  function Coll : TGameIntfPartsColl ;
      //constructor Create(Collection : TCollection); override;
      //Destructor Destroy; override ;
      //procedure SetCollection (aColl: TGameImgObjColl);
      //property Graphic : TGraphic  read GetGraphic write SetGraphic;
      //property ImgColl : TGameImgObjColl read  GetImgColl write SetCollection;
      //property Mask : TMask read fMask write SetMask;
    function Coll: TGameIntfPartsColl; virtual;
    procedure MouseUp(Shift: tShiftState); virtual;
    procedure MouseDown(Shift: tShiftState); virtual;
    procedure Mouseenter(Shift: tShiftState); virtual;
    procedure Mouseleave(Shift: tShiftState); virtual;
    procedure GrafDefinesSize; virtual; abstract;
    property ImgObj: TGameImgIntf read GetImgObj write SetImgObj;

    property xCord: Word read GetxCord write SetxCord;
    property yCord: Word read GetyCord write SetyCord;
    property gipWidth: Word read FgipWidth write SetgipWidth;
    property gipHeight: Word read FgipHeight write SetgipHeight;
  end;

  //------------------------------------------------------------------------------

  TGameIntfAnim = class(TGameIntfPart)
    function GetImgObj(): tGameImgAnim;
    procedure SetImgObj(const Value: tGameImgAnim);

  public
    constructor Create(Collection: TCollection); override;
    procedure GrafDefinesSize; override;
    property ImgObj: tGameImgAnim read GetImgObj write SetImgObj;
  end;
  //------------------------------------------------------------------------------
  TGameIntfBack = class(TGameIntfPart);

  TGameIntfField = class(TGameIntfBack) // DO NOT create objects of this class
  private
    FLeftCol, FTopRow, FVisCol, FVisRow: TCellInt;
    function GetVisCol: TCellInt;
    function GetVisRow: TCellInt;

  protected
    FCurCoords: TPoint;
    FHeight, FWidth: TCellInt;
    function GetHeight: TCellInt; virtual; abstract;
    function GetWidth: TCellInt; virtual; abstract;

    function GetImgObj(): tGameImgMap;
    procedure SetImgObj(const Value: tGameImgMap);
    procedure SetgipWidth(const Value: Word); override;
    procedure SetgipHeight(const Value: Word); override;
    function GetViewZone(): TRect;

    procedure SetLeftCol(const Value: TCellInt);
    procedure SetTopRow(const Value: TCellInt);
    procedure SetVisCol(const Value: TCellInt);
    procedure SetVisRow(const Value: TCellInt);
    procedure SetViewZone(const Value: tRect);

    procedure SetCurCoords(const Value: TPoint); virtual;
  public
    procedure GetCellGrafic(abmp: TBmp; ay, ax: TCellInt); virtual; abstract;
    procedure SetView(ay, ax: TCellInt);
    function Scroll(aDir: NdiRection): Boolean;
    property Height: TCellInt read GetHeight write FHeight;
    property Width: TCellInt read GetWidth write FWidth;
    property ImgObj: tGameImgMap read GetImgObj write SetImgObj;

    property ViewZone: tRect read GetViewZone write SetViewZone;
    property LeftCol: TCellInt read FLeftCol write SetLeftCol;
    property TopRow: TCellInt read FTopRow write SetTopRow;
    property VisCol: TCellInt read GetVisCol write SetVisCol;
    property VisRow: TCellInt read GetVisRow write SetVisRow;
    property CurCoords: TPoint read FCurCoords write SetCurCoords;

  end;
  //------------------------------------------------------------------------------

  TGameIntfLstItem = class(TGameIntfPart)
  private
    fnum: Word;
    fLst: TGameIntfLstColl;
    fText: string;
  protected
    function GetSel(): Boolean;
    procedure SetSel(Sel: Boolean);
    function GetImgObj(): TGameImgLstItem;
    procedure SetImgObj(const Value: TGameImgLstItem);
    function GetxCord: Word; override;
    function GetyCord: Word; override;
    procedure SetText(const Value: string);
  public
    space: Integer;
    Img: TBmp;
    ItemFontCol: TColor;
    //Font : TFont;
    function Lst: TGameIntfLst;
    procedure MouseUp(Shift: tShiftState); override;
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    function Coll: TGameIntfPartsColl; override;
    property Selected: Boolean read GetSel write SetSel;
    property ImgObj: TGameImgLstItem read GetImgObj write SetImgObj;
    property Text: string read fText write SetText;
  end;

  //------------------------------------------------------------------------------

  TGameIntfLstColl = class(TList)
  private
    FOwnr: TGameIntfLst;
    function Get(Index: Integer): TGameIntfLstItem;
    procedure Put(Index: Integer; const Value: TGameIntfLstItem);
    function AddItem(): TGameIntfLstItem;
  public
    constructor Create(AOwnr: TGameIntfLst);
    function Add(): TGameIntfLstItem;
    function insert(Index: Integer): TGameIntfLstItem;
    property Items[Index: Integer]: TGameIntfLstItem read Get write Put;
    default;
    property Ownr: TGameIntfLst read FOwnr;
  end;

  //------------------------------------------------------------------------------

  TGameIntfLst = class(TGameIntfPart)
  private
    FLst: TGameIntfLstColl;
    FFont: TFont;
    fCurSel: Integer;
    FOnSelChange: TNotifyEvent;
  protected
    procedure SetOnSelChange(const Value: TNotifyEvent);
    function Get(Index: Integer): TGameIntfLstItem;
    procedure Put(Index: Integer; const Value: TGameIntfLstItem);
    procedure SetFontCol(const Value: TColor);
    function GetFontCol: TColor;
    procedure SetFontFace(const Value: TFontName);
    function GetFontFace: TFontName;
    procedure SetFontSize(const Value: Integer);
    function GetFontSize: Integer;
    procedure SetFontStyle(const Value: TFontStyles);
    function GetFontStyle: TFontStyles;
    function GetImgObj(): TGameImgLst;
    function GetItemXCord(Num: Integer): Word;
    function GetItemYCord(Num: Integer): Word;
    procedure SetImgObj(const Value: TGameImgLst);
    procedure SetFont(const Value: TFont);
    procedure SetCurSel(const Value: Integer);
  public
    Selectable: Boolean;
    SelCol: TColor;
    Interval: Integer;
    Layer: TBmp;
    ItemBg: TBmp;
    BorderOfs: Integer;
    DrawLayerIfListEmpty: Boolean;
    DrawWideItemBg: Boolean;
    constructor Create(Collection: TCollection); override;
    destructor Destroy(); override;
    function Count: Integer;
    function AddRow(aText: string): TGameIntfLstItem;
    function AddMultiLineText(aText: string): Integer;
    procedure LoadItemBg(aPath: string);
    procedure LoadLayer(aPath: string);
    procedure MouseUp(Shift: tShiftState); override;
    procedure Clear();
    procedure ListKeyDown(Sender: TObject);
    procedure ListKeyUp(Sender: TObject);
    property Font: TFont write SetFont;
    property OnSelChange: TNotifyEvent read FOnSelChange write SetOnSelChange;
    property CurSel: Integer read fCurSel write SetCurSel;
    property Lst: TGameIntfLstColl read FLst;
    property Items[Index: Integer]: TGameIntfLstItem read Get write Put;
    default;
    property FontCol: TColor read GetFontCol write SetFontCol;
    property FontFace: TFontName read GetFontFace write SetFontFace;
    property FontSize: Integer read GetFontSize write SetFontSize;
    property FontStyle: TFontStyles read GetFontStyle write SetFontStyle;
  end;

  //------------------------------------------------------------------------------
  TGameIntfBkgdClass = class of TGameIntfBkgdcommon;

  TGameIntfBkgdcommon = class(TGameIntfBack)
  protected
    function GetImgObj(): TGameImgBkgd;
    procedure SetImgObj(const Value: TGameImgBkgd);
  public
    property ImgObj: TGameImgBkgd read GetImgObj write SetImgObj;
  end;

  TGameIntfDlgBkgd = class(TGameIntfBkgdcommon)
    constructor Create(Collection: TCollection); override;
  end;

  TGameIntfBkgd = class(TGameIntfBkgdcommon)
  end;

  TGameIntfBkgdSimple = class(TGameIntfBkgd)
    constructor Create(Collection: TCollection); override;
  end;

  TGameIntfPavedBkgd = class(TGameIntfBkgd)
  protected
    function GetImgObj(): TGameImgPavedBkgd;
    procedure SetImgObj(const Value: TGameImgPavedBkgd);
  public
    constructor Create(Collection: TCollection); override;
    property ImgObj: TGameImgPavedBkgd read GetImgObj write SetImgObj;
  end;

  TGameIntfFlipedBkgd = class(TGameIntfBkgd)
  protected
    function GetImgObj(): TGameImgFlipedBkgd;
    procedure SetImgObj(const Value: TGameImgFlipedBkgd);
  public
    constructor Create(Collection: TCollection); override;
    property ImgObj: TGameImgFlipedBkgd read GetImgObj write SetImgObj;
  end;

  TGameIntfStretchedBkgd = class(TGameIntfBkgd)
  protected
    function GetImgObj(): TGameImgStretchedBkgd;
    procedure SetImgObj(const Value: TGameImgStretchedBkgd);
  public
    constructor Create(Collection: TCollection); override;
    property ImgObj: TGameImgStretchedBkgd read GetImgObj write SetImgObj;
  end;
  //------------------------------------------------------------------------------

  NButtonState = (bsNormal, bsHover, bsLeftPush, bsMiddlePush, bsRightPush);

  TGameIntfBtn = class(TGameIntfPart, IKeyUsable)
  private
    fCurState: NButtonState;
    procedure SetCurState(const Value: NButtonState);
  protected
    procedure postprocess(); virtual;
  public
    // TODO Make frame groUps
    Imglist: Timagelist; // storage for Graphics
    //procedure SetBmp (aBmp: TBmp);
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure MouseUp(Shift: tShiftState); override;
    procedure MouseDown(Shift: tShiftState); override;
    procedure Mouseenter(Shift: tShiftState); override;
    procedure Mouseleave(Shift: tShiftState); override;
    procedure GrafDefinesSize; override;
    //property Curframe : Word read fCurframe write SetCurframe;
    //property Graphic : TGraphic  read GetGraphic {write SetGraphic};
    property CurState: NButtonState read fCurState write SetCurState;
  end;

  TGameIntfTextBtn = class(TGameIntfBtn)
  private
    FFont: TFont;
    FTextShift: Integer;
    Fcaption: string; // should be Loaded from config
    procedure SetFont(const Value: TFont);
    procedure SetTextShift(const Value: Integer);
    procedure Setcaption(const Value: string);
  protected
    procedure postprocess(); override;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    property TextShift: Integer read FTextShift write SetTextShift;
    property Font: TFont read fFont write SetFont;
    property Caption: string read Fcaption write Setcaption;
  end;

  //------------------------------------------------------------------------------

  TGameIntfPartsColl = class(TCollection)
    // holds all GameObjects to interact via Pixeng
  private
    FGamePix: TGamePixels;
    function GetItem(Index: Integer): TGameIntfPart;
    procedure SetItem(Index: Integer; const Value: TGameIntfPart);
  public
    function Add(): TGameIntfPart;
    constructor Create(aGamePix: TGamePixels);
    destructor Destroy; override;
    property Items[Index: Integer]: TGameIntfPart read GetItem write SetItem;
    default;
    property GamePix: TGamePixels read FGamePix;
  end;

  //-------------------------dialog property GamePix: TGamePixels read FGamePix;ues--------------------------------------------

  TDlgClass = class of TsimpleDlg;

  NDlgResult = (drNone, drYes, drNo);

  TDlgcallbackproc = procedure(ares: NDlgResult) of object;

  TsimpleDlg = class(TObject)
  private
    FGamePix: TGamePixels;
    felemCount: Integer;
    fBkg: TGameIntfDlgBkgd;
    Fdlgtext: string;
    fcallback: TDlgcallbackproc;
  protected
    procedure CreateBkg(aBmp: TBmp);
  public
    constructor Create(aGamePix: TGamePixels); //virtual;
    destructor Destroy(); override;
    function buildDlg(DlgText: string; acallback: TDlgcallbackproc): TsimpleDlg;
      virtual;
    procedure killDlg();
    procedure KeyPush(key: Word); virtual;
    property GamePix: TGamePixels read FGamePix;
  end;

  TTextDlg = class(TsimpleDlg)
  private
    flist: TGameIntfLst;
  protected
    procedure CreateText(DlgText: string);
  public
    function buildDlg(DlgText: string; acallback: TDlgcallbackproc): TsimpleDlg;
      override;
  end;

  PGameIntfBtn = ^TGameIntfBtn;

  TOkBtnDlg = class(TTextDlg)
  private
    fokBtn: TGameIntfBtn;
    procedure Mouseevent(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; x, y: Integer); virtual;
  protected
    procedure CreateBtn(aBtn: PGameIntfBtn; aGraf: TGrafres;
      yCord, xCord: Integer);
  public
    function buildDlg(DlgText: string; acallback: TDlgcallbackproc): TsimpleDlg;
      override;
    procedure KeyPush(key: Word); override;
  public

  end;

  TYesnoDlg = class(TokBtnDlg)
  private
    YesBtn, noBtn: TGameIntfBtn;
    procedure Mouseevent(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; x, y: Integer); override;
  protected
    procedure CreateBtn(aBtn: PGameIntfBtn; aGraf: TGrafres; yCord, xCord:
      Integer);
  public
    function buildDlg(DlgText: string; acallback: TDlgcallbackproc): TsimpleDlg;
      override;
    procedure KeyPush(key: Word); override;
  end;

  //------------------------------------------------------------------------------

  Pixelsar = array of array of TGameImgObj;

  TGamePixels = class(TObject)
  private
    FMainBuf, FBkgdBuf: TBmp;
    gPixels: Pixelsar;
    fGame: TObject;
    FImgObjColl: TGameImgObjColl;
    CurPixelObj: TGameImgObj;
    FGPWidth, FGPHeight: Word;
    FIntfPartsColl: TGameIntfPartsColl;
    FCursorPoint: TPoint;
    FCurShiftState: TShiftState;

    procedure Setdims(AHeight, AWidth: Word);
    procedure nilifyPixels();
    function GetPixels(Row, Col: Word): TGameImgObj;
  protected
    procedure PrepareBkg;
    function GetMainBuf: TBmp;
    procedure RemapItem(anItem: Integer);
    procedure FillBkg(Obj: TGameImgObj);
  public
    constructor Create(aGame: TObject);
    destructor Destroy; override;

    function AddBkgd(ay, ax, ah, aw: Word; aBmp: TBmp; AMode: NJobMode;
      ABkgdClass: TGameIntfBkgdClass): TGameIntfBkgdCommon;

    function AddBtn(ay, ax: Word; aGraf: TGrafRes): TGameIntfBtn;
    // Add here Gamefunction of a button
    function AddTextBtn(ay, ax: Word; acaption: string; aFont: TFont):
      TGameIntfTextBtn;
    function AddLst(ay, ax: Word): TGameIntfLst;
    procedure Tick;
    procedure MouseDown(ay, ax: Word; Shift: tShiftState);
    procedure MouseMove(ay, ax: Word; Shift: tShiftState);
    procedure MouseUp(ay, ax: Word; Shift: tShiftState);
    procedure Remap;
    procedure Reset(AHeight, AWidth: Word);
    property gpHeight: Word read FGPHeight;
    property gpWidth: Word read FGPWidth;
    property MainBuf: TBmp read GetMainBuf;
    property Pixels[Row: Word; Col: Word]: TGameImgObj read GetPixels; default;
    property ImgObjColl: TGameImgObjColl read FImgObjColl;
    property IntfPartsColl: TGameIntfPartsColl read FIntfPartsColl;
    property CursorPoint: TPoint read FCursorPoint;
    property CurShiftState: TShiftState read FCurShiftState;
  end;

  //------------------------------------------------------------------------------

implementation

uses uSound;

procedure DbgBmp(Bmp: TBmp);
var
  fs: TFormatSettings;
  DbgPath: string;
begin
  fs.LongTimeFormat := 'hh.mm.ss.zzz';
  DbgPath := AppPath + 'dbgscr';
  if ForceDirectories(DbgPath) then
    Bmp.SaveToFile(Format('%s\%s.bmp', [DbgPath, TimeToStr(now, fs)]))
  else
    ; // add logging
end;

//------------------------------------------------------------------------------

{ TGamePixels }

constructor TGamePixels.Create(aGame: TObject);
begin
  inherited Create;
  fGame := aGame;
  fImgObjColl := TGameImgObjColl.Create(Self);
  fIntfPartsColl := TGameIntfPartsColl.Create(Self);
  FMainBuf := TBmp.Create;
  FBkgdBuf := TBmp.Create;
end;

destructor TGamePixels.Destroy;
begin
  fGame := nil;
  nilifyPixels();
  gPixels := nil;
  FreeAndNil(fImgObjColl);
  FreeAndNil(fIntfPartsColl);
  FreeAndNil(fMainBuf);
  FreeAndNil(fBkgdBuf);
  inherited;
end;

function TGamePixels.AddBtn(ay, ax: Word; aGraf: TGrafRes): TGameIntfBtn;
begin
  Result := TGameIntfBtn.Create(IntfPartsColl);
  Result.Imglist := aGraf.ImgLst;
  Result.yCord := ay;
  Result.xCord := ax;
  Result.CurState := bsNormal;
end;

function TGamePixels.AddTextBtn(ay, ax: Word; acaption: string;
  aFont: TFont): TGameIntfTextBtn;
begin
  Result := TGameIntfTextBtn.Create(IntfPartsColl);
  Result.yCord := ay;
  Result.xCord := ax;
  Result.caption := acaption;
  Result.Font := aFont;
  Result.TextShift := 1;
end;

function TGamePixels.AddBkgd(ay, ax, ah, aw: Word; aBmp: TBmp; AMode:
  NJobMode; ABkgdClass: TGameIntfBkgdClass): TGameIntfBkgdCommon;
begin
  Result := ABkgdClass.Create(IntfPartsColl);
  Result.yCord := ay;
  Result.xCord := ax;
  Result.gipHeight := ah;
  Result.gipWidth := aw;
  Result.ImgObj.FbmpIn := aBmp;
  Result.ImgObj.JobMode := AMode;
end;

function TGamePixels.AddLst(ay, ax: Word): TGameIntfLst;
begin
  Result := TGameIntfLst.Create(IntfPartsColl);
  Result.yCord := ay;
  Result.xCord := ax;
end;

procedure TGamePixels.nilifyPixels();
var
  i, j: Word;
begin
  if gPixels = nil then
    Exit;
  for i := 0 to gpHeight - 1 do
    for j := 0 to gpWidth - 1 do
      gPixels[i, j] := nil;
end;

procedure TGamePixels.Reset(AHeight, AWidth: Word);
  procedure resetbuf(abmp: TBmp);
  begin
    abmp.ReleaseHandle;
    abmp.Height := AHeight;
    abmp.Width := AWidth;
  end;
begin
  Setdims(AHeight, AWidth);
  resetbuf(Fmainbuf);
  resetbuf(FBkgdBuf);
  ImgObjColl.Clear; // TODO here make save-load of collections
  IntfPartsColl.Clear;
  CurPixelObj := nil;
end;

procedure TGamePixels.Setdims(AHeight, AWidth: Word);
begin
  nilifyPixels();
  SetLength(gPixels, AHeight, AWidth);
  FGPWidth := AWidth;
  FGPHeight := AHeight;
end;

function TGamePixels.GetPixels(Row, Col: Word): TGameImgObj;
begin
  if (Row >= gpHeight) or (Col >= gpWidth) then
    raise ERangeError.Create('wrong pointer to GameObj');
  Result := gPixels[Row, Col];
end;

procedure TGamePixels.Remap;
var
  i: Word;
begin
  // refill the Collection by calling Game.
  nilifyPixels;
  if ImgObjColl.Count = 0 then
    Exit;
  PrepareBkg;
  FMainBuf.Assign(FBkgdBuf);
  for i := 0 to ImgObjColl.Count - 1 do
    RemapItem(i);

end;

procedure TGamePixels.RemapItem(anItem: Integer);
var
  i, j: Word;
  Item: TGameImgObj;
begin
  Item := ImgObjColl.Items[anItem];
  if (Item.Mask <> nil) and (Item.Mask.MskWidth <> 0)
    and (Item.Mask.MskHeight <> 0) then
  begin
    i := 0;
    while i < Item.Mask.MskHeight do
    begin
      j := 0;
      while j < Item.Mask.MskWidth do
      begin
        if Item.Mask.Cells[i, j] then
          gPixels[i + Item.yCord, j + Item.xCord] := Item
        else
          gPixels[i + Item.yCord, j + Item.xCord] := nil;
        // Make here Resetting of old Objects
        inc(j);
      end;
      inc(i);
    end;
  end;
end;

procedure TGamePixels.MouseDown(ay, ax: Word; Shift: tShiftState);
begin
  if (Pixels[ay, ax] <> nil) then
    Pixels[ay, ax].MouseDown(Shift);
end;

procedure TGamePixels.MouseMove(ay, ax: Word; Shift: tShiftState);
begin
  FCursorPoint := Point(ax, ay);
  FCurShiftState := Shift;
  if (Pixels[ay, ax] <> CurPixelObj) then
  begin
    if (CurPixelObj <> nil) then
      CurPixelObj.Mouseleave(Shift);
    if (Pixels[ay, ax] <> nil) then
      Pixels[ay, ax].Mouseenter(Shift);
  end
  else if CurPixelObj <> nil then
    CurPixelObj.MouseMove(ay, ax, Shift);
  CurPixelObj := Pixels[ay, ax];
end;

procedure TGamePixels.MouseUp(ay, ax: Word; Shift: tShiftState);
begin
  if (Pixels[ay, ax] <> nil) then
    Pixels[ay, ax].MouseUp(Shift);
end;

function TGamePixels.GetMainBuf: TBmp;
var
  i: Word;
  b: TBitmap;
begin
  //Assert(ImgObjColl.Count <> 0, stremptyBuf);
  Result := nil;
  with ImgObjColl do
  begin
    if Count = 0 then
      Exit;
    for i := 0 to Count - 1 do
      FillBkg(Items[i]);
    for i := 0 to Count - 1 do
      with Items[i] do
        if not (Ownr is TGameIntfBkgd) and NeedDraw { and not ActDisabled} then
        begin
          fMainBuf.Canvas.Draw(xCord, yCord, Graphic);
          IsChanged := False;
        end;
    Result := fMainBuf;
  end;


  b := TBitmap.Create;
  b.LoadFromFile(GrafPath + '1.bmp');
  b.Transparent := True;
  Result.Canvas.Draw(0, 0, b);
  b.free;
end;

//------------------------------------------------------------------------------

procedure TGamePixels.PrepareBkg;
var
  i: Integer;
begin
  for i := 0 to ImgObjColl.Count - 1 do
    with ImgObjColl.Items[i] do
      if (Ownr is TGameIntfBack) {and IsChanged} then
        FBkgdBuf.Canvas.Draw(xCord, yCord, Graphic);
  //FMainBuf.Assign(FBkgdBuf);
end;

procedure TGamePixels.Tick;
begin
  ImgObjColl.Tick;
end;

procedure TGamePixels.FillBkg(Obj: TGameImgObj);
var
  rct: TRect;
begin
  with Obj do
    if not (Ownr is TGameIntfBkgdcommon) and NeedDraw then
    begin
      rct := Rect(xCord, yCord, xCord + gioWidth, yCord + gioHeight);
      CopyRect(fMainBuf.Canvas, FBkgdBuf.Canvas, rct, rct);
      //DbgBmp(FMainBuf);
    end;
end;

//------------------------------------------------------------------------------

{ TGameImgIntf }

constructor TGameImgIntf.Create(Collection: TCollection);
begin
  inherited;
  fBmp := TBmp.Create;
  fBmp.Transparent := True;
  fBmp.Canvas.Brush.Style := bsClear;
  IsChanged := True;
  //    fBmp.TransparentColor := clWhite;
end;

destructor TGameImgIntf.Destroy;
begin
  FreeAndNil(fbmp);
  inherited;
end;

procedure TGameImgIntf.SetGraphic(const Value: TGraphic);
begin
  //Bmp := TBmp.Create;
  fBmp.Assign(Value);
end;

function TGameImgIntf.GetGraphic: TGraphic;
begin
  Result := fBmp;
end;

procedure TGameImgIntf.MouseDown(Shift: tShiftState);
begin
  if ActDisabled then
    Exit;
  Ownr.MouseDown(Shift);
  IsChanged := True;
end;

procedure TGameImgIntf.Mouseenter(Shift: tShiftState);
begin
  if ActDisabled then
    Exit;
  Ownr.Mouseenter(Shift);
  IsChanged := True;
end;

procedure TGameImgIntf.Mouseleave(Shift: tShiftState);
begin
  inherited;
  if ActDisabled then
    Exit;
  Ownr.Mouseleave(Shift);
  IsChanged := True;
end;

procedure TGameImgIntf.MouseUp(Shift: tShiftState);
begin
  if ActDisabled then
    Exit;
  Ownr.MouseUp(Shift);
  IsChanged := True;
  if Assigned(onMouse) then // call of external method must be last in calls
    onMouse(Self, mbLeft, Shift, 0, 0);
end;

procedure TGameImgIntf.SetBmp(aBmp: TBmp; aTransCol: Cardinal = clwhite);
var
  i, j: Word;
  PPix: prgbarray;
begin
  Assert((aBmp.Height <> 0) and (aBmp.Width <> 0), strwrongBmp);
  //  fBmp.Width := aBmp.Width;
  //  fBmp.Height := aBmp.Height;
  //  fBmp.Canvas.Draw(0,0, abmp);
    //aBmp.TransparentColor := 0;
  fBmp.Assign(abmp);
  fBmp.Transparent := True;
  //  fBmp.TransparentColor := -$7fffffff;
  //  fBmp.TransparentColor := fBmp.TransparentColor+1;
  fBmp.TransparentColor := {Integer}abmp.Canvas.Pixels[0,0];//(aTransCol);
  fBmp.TransparentMode := tmfixed;

  fMask.Reset(aBmp.Height, aBmp.Width);
  for i := 0 to aBmp.Height - 1 do
  begin
    PPix := aBmp.ScanLine[i];
    for j := 0 to aBmp.Width - 1 do
      fMask.Cells[i, j] := not MatchCol(PPix^[j], aTransCol);
    //        aBmp.Canvas.Pixels[j,i{X first !}] <> aTransCol;
  end;
  GamePix.RemapItem(Index);
end;

//------------------------------------------------------------------------------

{ TGameImgObj }

function TGameImgObj.GetXCord: Word;
begin
  Result := Ownr.xCord;
end;

function TGameImgObj.GetYCord: Word;
begin
  Result := Ownr.yCord;
end;

constructor TGameImgObj.Create(Collection: TCollection);
begin
  inherited;
  fMask := TMask.Create;
end;

destructor TGameImgObj.Destroy;
begin
  FreeAndNil(fMask);
  FreeAndNil(FMover);
  inherited;
end;

function TGameImgObj.GetgioHeight: Word;
begin
  Result := Ownr.gipHeight;
end;

function TGameImgObj.GetgioWidth: Word;
begin
  Result := Ownr.gipWidth;
end;

function TGameImgObj.GetImgColl(): TGameImgObjColl;
begin
  Result := Collection as TGameImgObjColl;
end;

procedure TGameImgObj.SetOwnr(const Value: TGameIntfPart);
begin
  FOwnr := Value;
end;

procedure TGameImgObj.SetxCord(const Value: Word);
begin
  if Ownr.FxCord = Value then
    Exit;
  IsChanged := True;
  Gamepix.FillBkg(Self);
  Ownr.xCord := Value;
end;

procedure TGameImgObj.SetyCord(const Value: Word);
begin
  if Ownr.FYCord = Value then
    Exit;
  IsChanged := True;
  Gamepix.FillBkg(Self);
  Ownr.yCord := Value;
end;

function TGameImgObj.GetOwnr: TGameIntfPart;
begin
  Result := FOwnr;
end;

procedure TGameImgObj.SetColl(aCollection: TGameImgObjColl);
begin
  SetCollection(aCollection);
end;

procedure TGameImgObj.SetgioHeight(const Value: Word);
begin
  FgioHeight := Value;
end;

procedure TGameImgObj.SetgioWidth(const Value: Word);
begin
  FgioWidth := Value;
end;

procedure TGameImgObj.SetMask(AMask: TMask);
begin
  //  if fMask = nil then
  //    fMask := tMask.Create;
  fMask.Reset(AMask.MskHeight, AMask.MskWidth);
end;

//------------------------------------------------------------------------------

function TGameImgObj.NeedDraw: Boolean;
begin
  Result := not (DrawDisabled) and IsChanged;
end;

procedure TGameImgObj.MouseDown(Shift: tShiftState);
begin

end;

procedure TGameImgObj.Mouseenter(Shift: tShiftState);
begin

end;

procedure TGameImgObj.Mouseleave(Shift: tShiftState);
begin

end;

procedure TGameImgObj.MouseMove(ay, ax: Word; Shift: tShiftState);
begin

end;

procedure TGameImgObj.MouseUp(Shift: tShiftState);
begin

end;

function TGameImgObj.ClientX(ax: Word): Word;
begin
  Result := ax - xCord;
end;

function TGameImgObj.ClientY(ay: Word): Word;
begin
  Result := ay - yCord;
end;

procedure TGameImgObj.Tick;
var
  Pnt: TPoint;
begin
  if not ActDisabled and Assigned(OnTick) then
    OnTick(Self);
  if Assigned(Mover) and Mover.Move(Pnt) then
  begin
    WXCord := xCord + Pnt.x;
    WYCord := yCord + Pnt.y;
  end;
end;

procedure TGameImgObj.SetChanged(const Value: Boolean);
begin
  if FLockCnt = 0 then
    FChanged := Value;
end;

function TGameImgObj.Gamepix: TGamePixels;
begin
  Result := (Collection as TGameImgObjColl).GamePix
end;

procedure TGameImgObj.SetOnTick(const Value: TNotifyEvent);
begin
  FOnTick := Value;
end;

procedure TGameImgObj.SetMover(const Value: TMover);
begin
  FreeAndNil(FMover);
  FMover := Value;
end;

procedure TGameImgObj.Lock;
begin
  inc(FLockCnt);
end;

procedure TGameImgObj.UnLock;
begin
  FLockCnt := Max(0, FLockCnt - 1);
end;

{ TGameImgObjColl }

function TGameImgObjColl.Add(): TGameImgObj;
begin
  Result := (inherited Add) as TGameImgObj;
end;

constructor TGameImgObjColl.Create(aGamePix: TGamePixels);
begin
  inherited Create(TGameImgObj);
  fGamePix := aGamePix;
end;

procedure TGameImgObjColl.activate(b: Boolean);
var
  i: Integer;
begin
  if Count <> 0 then
    for i := 0 to Count - 1 do
    begin
      Items[i].ActDisabled := b;
      Items[i].IsChanged := True;
    end;
end;

function TGameImgObjColl.GetItem(Index: Integer): TGameImgObj;
begin
  Result := inherited GetItem(Index) as TGameImgObj
end;

procedure TGameImgObjColl.SetItem(Index: Integer; const Value: TGameImgObj);
begin
  inherited SetItem(Index, Value);
end;

//------------------------------------------------------------------------------

procedure TGameImgObjColl.dellast;
begin
  if GamePix.CurPixelObj = Items[Count - 1] then
    GamePix.CurPixelObj := nil;

  Delete(Count - 1);

end;

procedure TGameImgObjColl.Tick;
var
  i: Word;
begin
  i := 0;
  while i < count do
  begin
    Items[i].tick;
    inc(i);
  end;
end;

{ TMask }

procedure TMask.Reset(AHeight, AWidth: Word; Val: Boolean = False);
var
  i, j: Word;
begin
  if (FMskHeight <> AHeight) or (FMskWidth <> AWidth) then
    SetLength(fCells, AHeight, AWidth);
  FMskHeight := AHeight;
  FMskWidth := AWidth;
  if (AHeight = 0) or (AWidth = 0) then
    Exit;
  for i := 0 to AHeight - 1 do
    for j := 0 to AWidth - 1 do
      fCells[i, j] := Val;
end;

destructor TMask.Destroy;
begin
  fCells := nil;
  inherited;
end;

//------------------------------------------------------------------------------

{ TGameIntfPart }
{
function TGameIntfPart.Coll: TGameIntfPartsColl;
begin
Result := Collection as TGameIntfPartsColl ;
end; }

procedure TGameIntfPart.AfterConstruction;
begin
  inherited;

end;

procedure TGameIntfPart.BeforeDestruction;
begin
  inherited;

end;

function TGameIntfPart.Coll: TGameIntfPartsColl;
begin
  Result := Collection as TGameIntfPartsColl;
end;

{constructor TGameIntfPart.Create(Collection: TCollection);
begin
  inherited;

end;}

function TGameIntfPart.GetImgObj: TGameImgIntf;
begin
  Result := fImgObj;
end;

function TGameIntfPart.GetxCord: Word;
begin
  Result := fxCord;
end;

function TGameIntfPart.GetyCord: Word;
begin
  Result := fyCord;
end;

//procedure TGameIntfPart.GrafDefinesSize;
//begin
//
//end;

procedure TGameIntfPart.KeyPress(Key: Word; Shift: TShiftState);
begin
  if (FKey = Key) and (Fshift = Shift) and Assigned(FEvent) then
    FEvent(Self);
end;

procedure TGameIntfPart.MouseDown(Shift: tShiftState);
begin

end;

procedure TGameIntfPart.Mouseenter(Shift: tShiftState);
begin

end;

procedure TGameIntfPart.Mouseleave(Shift: tShiftState);
begin

end;

procedure TGameIntfPart.MouseUp(Shift: tShiftState);
begin

end;

function TGameIntfPart.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TGameIntfPart._AddRef: Integer;
begin
  Result := InterlockedIncrement(FRefCount);
end;

function TGameIntfPart._Release: Integer;
begin
  Result := InterlockedDecrement(FRefCount);
  //  if Result = 0 then
  //    Destroy;
end;

procedure TGameIntfPart.RegKey(Key: Word; Shift: TShiftState; Event:
  TNotifyEvent);
begin
  Fkey := Key;
  Fshift := Shift;
  FEvent := Event;
end;

procedure TGameIntfPart.SetgipHeight(const Value: Word);
begin
  FgipHeight := Value;
  ImgObj.wgioHeight := Value;
end;

procedure TGameIntfPart.SetgipWidth(const Value: Word);
begin
  FgipWidth := Value;
  ImgObj.wgioWidth := Value;
end;

procedure TGameIntfPart.SetImgObj(const Value: TGameImgIntf);
begin
  Assert(FImgObj = nil, '����');
  //if FImgObj <> nil then
  //  FImgObj := Value
  //else
  FImgObj := Value;
end;

procedure TGameIntfPart.SetxCord(const Value: Word);
begin
  FxCord := Value;
  //ImgObj.wxCord := Value;
end;

procedure TGameIntfPart.SetyCord(const Value: Word);
begin
  FyCord := Value;
  //ImgObj.wyCord := Value;
end;

//------------------------------------------------------------------------------

{ TGameIntfBtn }

constructor TGameIntfBtn.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  //Imglist := TImageList.Create(nil);
  ImgObj := TGameImgIntf.Create(Coll.GamePix.ImgObjColl);
  //ImgObj.Bmp := TBmp.Create;
  ImgObj.Ownr := Self;
end;

destructor TGameIntfBtn.Destroy;
begin
  //  FreeAndNil(Imglist);
  inherited;
end;

procedure TGameIntfBtn.GrafDefinesSize;
begin
  gipWidth := Imglist.Width;
  gipHeight := Imglist.Height;
end;

procedure TGameIntfBtn.MouseDown(Shift: tShiftState);
begin
  if ssLeft in Shift then
    CurState := bsLeftPush;
  if ssmiddle in Shift then
    CurState := bsMiddlePush;
  if ssRight in Shift then
    CurState := bsRightPush;
end;

procedure TGameIntfBtn.Mouseenter(Shift: tShiftState);
begin
  CurState := bsHover;
end;

procedure TGameIntfBtn.Mouseleave(Shift: tShiftState);
begin
  CurState := bsNormal;
end;

procedure TGameIntfBtn.MouseUp(Shift: tShiftState);
begin
  CurState := bsHover;
  Sound.Play(mcClick);
end;

procedure TGameIntfBtn.postprocess;
begin
end;

procedure TGameIntfBtn.SetCurState(const Value: NButtonState);
var
  aBmp: TBmp;
begin
  aBmp := TBmp.Create;
  //  aBmp.Transparent := True;
  //  aBmp.TransparentMode := tmFixed;
  //  aBmp.Transparentcolor := Imglist.BlendColor;
  fCurState := Value;
  Imglist.getbitmap(Ord(fCurState), aBmp);
  ImgObj.SetBmp(aBmp, Imglist.BlendColor);
  postprocess;
  FreeAndNil(aBmp);
end;

//------------------------------------------------------------------------------

{ TGameIntfPartsColl }

function TGameIntfPartsColl.Add: TGameIntfPart;
begin
  Result := (inherited Add) as TGameIntfPart;
end;

constructor TGameIntfPartsColl.Create(aGamePix: TGamePixels);
begin
  inherited Create(TGameIntfPart);
  FGamePix := aGamePix;
end;

destructor TGameIntfPartsColl.Destroy;
begin
  fGamePix := nil;
  inherited;
end;

function TGameIntfPartsColl.GetItem(Index: Integer): TGameIntfPart;
begin
  Result := (inherited GetItem(Index)) as TGameIntfPart;
end;

procedure TGameIntfPartsColl.SetItem(Index: Integer; const Value:
  TGameIntfPart);
begin
  inherited SetItem(Index, Value);
end;

//------------------------------------------------------------------------------

{ TGameIntfLst }

// inscribe words into ImgObj Sizes breaking into lines

function TGameIntfLst.AddMultiLineText(aText: string): Integer;
var
  i, c, res: Word;
  sl: TStringList;
  s: string;
  function Addline(astr, aword: string): Boolean;
  begin
    Result := ImgObj.fBmp.Canvas.TextWidth(astr + aword) >= gipWidth - 2 *
      borderofs;
    if Result then
    begin
      AddRow(astr);
      inc(res)
    end;
  end;
  procedure WordDivider;
  begin
    sl := TStringList.Create;
    repeat
      i := Pos('  ', aText);
      Delete(aText, i, 1); // kill all double-space
    until i = 0;
    SL.Delimiter := ' ';
    SL.DelimitedText := aText; // divide all Text into words
  end;
begin
  WordDivider;
  Res := 0;
  s := '';
  c := SL.Count - 1;
  for i := 0 to c do
  begin
    if Addline(s, sl[i]) then // if string fits then inscribe it
      s := ''; // and Clear it
    s := s + sl[i] + ' '; // and Add a word
    if (i = c) and (s <> '') then // if needed Add last string
    begin
      AddRow(s);
      inc(res);
    end;
  end;
  Result := res;
  FreeAndNil(SL);
end;

function TGameIntfLst.AddRow(aText: string): TGameIntfLstItem;
begin
  Result := Lst.Add;
  Result.Text := aText;
  Result.ImgObj.SetBmp(nil); //Lst.layer
end;

//function TGameIntfLst.Coll: TGameIntfPartsColl;
//begin
//result := Collection as TGameIntfPartsColl;
//end;

function TGameIntfLst.GetFontCol: TColor;
begin
  Result := FFont.Color;
end;

procedure TGameIntfLst.SetFontCol(const Value: TColor);
var
  i: Integer;
begin
  if Value = FFont.Color then
    Exit;
  FFont.Color := Value;
  ImgObj.IsChanged := True;
  i := 0;
  while i < Count do
  begin
    Items[i].ImgObj.IsChanged := True;
    inc(i);
  end;
end;

constructor TGameIntfLst.Create;
begin
  inherited;
  ImgObj := TGameImgLst.Create(Coll.GamePix.ImgObjColl);
  //ImgObj.Bmp := TBmp.Create;
  ImgObj.Ownr := Self;
  FLst := TGameIntfLstColl.Create(Self);
  FFont := TFont.Create;
  //layer := TBmp.Create;
  //ItemBg := TBmp.Create;
end;

destructor TGameIntfLst.Destroy;
begin
  FreeAndNil(FLst);
  FreeAndNil(fFont);
  //FreeAndNil(layer);
  //FreeAndNil(ItemBg);
  inherited;
end;

function TGameIntfLst.Get(Index: Integer): TGameIntfLstItem;
begin
  Result := FLst.Get(Index)
end;

procedure TGameIntfLst.Put(Index: Integer; const Value: TGameIntfLstItem);
begin
  FLst.Put(Index, Value)
end;

function TGameIntfLst.GetImgObj: TGameImgLst;
begin
  Result := fImgObj as TGameImgLst;
end;

procedure TGameIntfLst.LoadItemBg(aPath: string);
begin
  Assert(FileExists(aPath), Format(strnoFile, [aPath]));
  LoadPngtoBmp(ItemBg, aPath);
end;

procedure TGameIntfLst.LoadLayer(aPath: string);
begin
  Assert(FileExists(aPath), Format(strnoFile, [aPath]));
  LoadPngtoBmp(layer, aPath);
end;

procedure TGameIntfLst.MouseUp(Shift: tShiftState);
begin
  inherited;
  if Selectable then
    if CurSel = 0 then
      CurSel := -1
    else
      CurSel := 0;

end;

procedure TGameIntfLst.SetImgObj(const Value: TGameImgLst);
begin
  inherited SetImgObj(Value);
end;

procedure TGameIntfLst.Clear;
begin
  Lst.Clear;
  ImgObj.IsChanged := True;
end;

function TGameIntfLst.Count: Integer;
begin
  Result := Lst.Count;
end;

procedure TGameIntfLst.SetCurSel(const Value: Integer);
begin
  if fCurSel = Value then
    Exit;
  Lst.Items[fCurSel].ImgObj.IsChanged := True;
  fCurSel := Value;
  Lst.Items[fCurSel].ImgObj.IsChanged := True;
  if Assigned(FOnSelChange) then
    FOnSelChange(nil); // may be not nil should be (possible TODO)
end;

procedure TGameIntfLst.SetOnSelChange(const Value: TNotifyEvent);
begin
  FOnSelChange := Value;
end;

//------------------------------------------------------------------------------

function TGameIntfLst.GetItemXCord(Num: Integer): Word;
begin
  Result := xCord + borderofs;
end;

function TGameIntfLst.GetItemYCord(Num: Integer): Word;
begin
  Result := yCord + borderofs + Num * (interval +
    Lst.Items[0].ImgObj.gioHeight);
end;

procedure TGameIntfLst.SetFont(const Value: TFont);
begin
  FFont.Assign(Value);
  ImgObj.fBmp.Canvas.Font := Value; // TODO refactor field usage
  ImgObj.fBmp.Canvas.Brush.Style := bsClear;
end;

procedure TGameIntfLst.SetFontFace(const Value: TFontName);
begin
  FFont.Name := Value;
end;

function TGameIntfLst.GetFontFace: TFontName;
begin
  Result := FFont.Name;
end;

procedure TGameIntfLst.SetFontSize(const Value: Integer);
begin
  FFont.Size := Value;
end;

function TGameIntfLst.GetFontSize: Integer;
begin
  Result := FFont.Size;
end;

procedure TGameIntfLst.SetFontStyle(const Value: TFontStyles);
begin
  FFont.Style := Value;
end;

function TGameIntfLst.GetFontStyle: TFontStyles;
begin
  Result := FFont.Style;
end;

procedure TGameIntfLst.ListKeyUp(Sender: TObject);
begin
  CurSel := Max(0, CurSel - 1);
end;

procedure TGameIntfLst.ListKeyDown(Sender: TObject);
begin
  CurSel := Min(Count - 1, CurSel + 1);
end;

//------------------------------------------------------------------------------

{ TGameIntfLstColl }

function TGameIntfLstColl.Add: TGameIntfLstItem;
begin
  Result := AddItem;
  inherited Add(Result);
  Result.fnum := Count - 1;
end;

function TGameIntfLstColl.AddItem: TGameIntfLstItem;
begin
  Result := TGameIntfLstItem.Create(Ownr.Coll);
  Result.fLst := Self;
  Result.ImgObj := TGameImgLstItem.Create(Ownr.ImgObj.ImgColl);
  Result.ImgObj.Ownr := Result;
end;

constructor TGameIntfLstColl.Create(AOwnr: TGameIntfLst);
begin
  inherited Create();
  FOwnr := AOwnr;
end;

function TGameIntfLstColl.Get(Index: Integer): TGameIntfLstItem;
begin
  Result := TGameIntfLstItem(inherited Get(Index));
end;

function TGameIntfLstColl.insert(Index: Integer): TGameIntfLstItem;
begin
  Result := AddItem;
  inherited insert(Index, Result);
end;

procedure TGameIntfLstColl.Put(Index: Integer; const Value: TGameIntfLstItem);
begin
  inherited Put(Index, Value);
end;

//------------------------------------------------------------------------------

{ TGameIntfLstItem }

function TGameIntfLstItem.Coll: TGameIntfPartsColl;
begin
  Result := fLst.Ownr.Coll;
end;

constructor TGameIntfLstItem.Create(Collection: TCollection);
begin
  inherited;

  Img := TBmp.Create;

end;

destructor TGameIntfLstItem.Destroy;
begin
  FreeAndNil(Img);
  inherited;
end;

function TGameIntfLstItem.GetImgObj: TGameImgLstItem;
begin
  Result := fImgObj as TGameImgLstItem;
end;

function TGameIntfLstItem.GetSel: Boolean;
begin
  Result := Lst.Selectable and (Lst.CurSel = fnum)
end;

function TGameIntfLstItem.GetxCord: Word;
begin
  Result := Lst.GetItemXCord(fnum);
end;

function TGameIntfLstItem.GetyCord: Word;
begin
  Result := Lst.GetItemyCord(fnum);
end;

function TGameIntfLstItem.Lst: TGameIntfLst;
begin
  Result := fLst.Ownr;
end;

procedure TGameIntfLstItem.MouseUp(Shift: tShiftState);
begin
  Selected := True;
  ImgObj.IsChanged := True;
end;

procedure TGameIntfLstItem.SetImgObj(const Value: TGameImgLstItem);
begin
  inherited SetImgObj(Value);
end;

procedure TGameIntfLstItem.SetSel(Sel: Boolean);
begin
  Lst.CurSel := fnum;
end;

procedure TGameIntfLstItem.SetText(const Value: string);
begin
  if Value = fText then
    Exit;
  fText := Value;
  ImgObj.IsChanged := True;
end;

//------------------------------------------------------------------------------

{ TGameImgLstItem }

function TGameImgLstItem.Coll: TGameImgObjColl;
begin
  Result := Collection as TGameImgObjColl;
end;

function TGameImgLstItem.GetgioHeight: Word;
begin
  if (fgioHeight = 0) and not (Ownr.Img.Empty and (Ownr.fText = '')) then
    fgioHeight := Graphic.Height;
  Result := fgioHeight;
end;

function TGameImgLstItem.GetgioWidth: Word;
begin
  if Ownr.Lst.DrawWideItemBg then
    fgioWidth := Ownr.Lst.gipWidth - 2 * Ownr.Lst.borderofs
  else if (fgioWidth = 0) and not (Ownr.Img.Empty and (Ownr.fText = '')) then
    fgioWidth := Graphic.Width;
  Result := fgioWidth;
end;

function TGameImgLstItem.GetGraphic: TGraphic;
begin
  with fBmp, Ownr do
  begin
    Width := 0; // claers existing bitmap data
    Canvas.Font.Assign(Lst.FFont); // TODO refactor field usage
    if Selected then
      Canvas.Font.Color := Lst.SelCol;
    Height := Max(Img.Height, Canvas.TextHeight(fText));
    if not Lst.DrawWideItemBg then
      Width := Img.Width + space + Canvas.TextWidth(fText)
    else
      Width := gioWidth;
    Canvas.StretchDraw(Rect(0, 0, Width, Height), Lst.ItemBg);
    Canvas.Draw(0, 0, Img);
    Canvas.TextOut(Img.Width + space, 0, fText);
  end; // with
  Result := fBmp;
end;

function TGameImgLstItem.GetOwnr: TGameIntfLstItem;
begin
  Result := fOwnr as TGameIntfLstItem;
end;

function TGameImgLstItem.GetXCord: Word;
begin
  Result := Ownr.xCord;
end;

function TGameImgLstItem.GetYCord: Word;
begin
  Result := Ownr.yCord;
end;

procedure TGameImgLstItem.SetBmp(aBmp: TBmp);
begin
  inherited SetBmp(Graphic as TBmp, clBlack);
end;

procedure TGameImgLstItem.SetChanged(const Value: Boolean);
begin
  inherited;
  if Value and Assigned(Ownr) then
    Ownr.Lst.ImgObj.IsChanged := Value;
end;

procedure TGameImgLstItem.SetOwnr(const Value: TGameIntfLstItem);
begin
  fOwnr := Value;
end;

//------------------------------------------------------------------------------

{ TGameImgLst }

function TGameImgLst.GetGraphic: TGraphic;
var
  i: Integer;
begin
  with fBmp, Ownr, Ownr.Lst do
    if Count > 0 then
    begin
      Height := 0;
      Width := 0;
      for i := 0 to Count - 1 do
      begin
        Height := Height + Items[i].ImgObj.gioHeight + interval;
        Width := Max(Width, Items[i].ImgObj.gioWidth);
      end;
      Width := borderofs + Width + borderofs;
      if interval > 10 then
        Height := borderofs +
          Height {+
        borderofs}//the last interval plays a role of a border
      else
        Height := borderofs +
          Height +
          borderofs;
      if (layer <> nil) and (not layer.Empty) then
        Canvas.StretchDraw(Rect(0, 0, Width, Height), layer);
      for i := 0 to Count - 1 do
        if i = 0 then
        begin
          Items[i].ImgObj.wxCord := xCord + borderofs;
          Items[i].ImgObj.wyCord := yCord + borderofs + i * interval;
        end
        else
        begin
          Items[i].ImgObj.wxCord := xCord + borderofs;
          Items[i].ImgObj.wyCord := yCord + borderofs +
            i * Items[i - 1].ImgObj.gioHeight + i * interval;
        end;
    end
    else if DrawLayerIfListEmpty then
      fBmp.Assign(layer);
  Result := fBmp;
end;

function TGameImgLst.GetOwnr: TGameIntfLst;
begin
  Result := fOwnr as TGameIntfLst;
end;

procedure TGameImgLst.SetOwnr(const Value: TGameIntfLst);
begin
  fOwnr := Value;
end;

//------------------------------------------------------------------------------

{ TsimpleDlg }

function TsimpleDlg.buildDlg(DlgText: string; acallback: TDlgcallbackproc):
  TsimpleDlg;
begin
  CreateBkg(ResMan.GraphicN[giDlgBkg].Bmp);
  Result := Self;
end;

constructor TsimpleDlg.Create(aGamePix: TGamePixels);
begin
  inherited Create;
  FGamePix := aGamePix;
  aGamePix.ImgObjColl.activate(True);
end;

procedure TsimpleDlg.CreateBkg(aBmp: TBmp);
begin
  with GamePix, abmp do
    fBkg := AddBkgd((gpHeight - Height) div 2, (gpWidth - Width) div 2,
      Height, Width, aBmp, jmNone, TGameIntfDlgBkgd) as TGameIntfDlgBkgd;
  inc(felemCount);
  fGamePix.PrepareBkg;
end;

destructor TsimpleDlg.Destroy;
begin
  killDlg;
  GamePix.ImgObjColl.activate(False);
  GamePix.PrepareBkg;
  fGamePix := nil;
  inherited;
end;

procedure TsimpleDlg.KeyPush(key: Word);
begin

end;

procedure TsimpleDlg.killDlg;
var
  i: Word;
begin
  if felemCount = 0 then
    Exit;
  for i := 0 to felemCount - 1 do
    GamePix.ImgObjColl.Dellast;

  GamePix.Remap;
end;

{ TYesnoDlg }

function TYesnoDlg.buildDlg(DlgText: string; acallback: TDlgcallbackproc):
  TsimpleDlg;
const
  Btntop = 0.7;
begin
  Fdlgtext := DlgText;
  fcallback := acallback;
  Assert(@fcallback <> nil, strnofunction);
  CreateBkg(ResMan.GraphicN[giDlgBkg].Bmp);
  CreateText(DlgText);
  CreateBtn(@YesBtn, ResMan.GraphicN[giEmptyBtn],
    fBkg.yCord + Trunc(fBkg.gipHeight * Btntop), fBkg.xCord
    + Trunc(fBkg.gipWidth * 0.1));
  CreateBtn(@noBtn, ResMan.GraphicN[giEmptyBtn],
    fBkg.yCord + Trunc(fBkg.gipHeight * Btntop), fBkg.xCord
    + Trunc(fBkg.gipWidth * 0.6));
  Result := Self;
end;

procedure TYesnoDlg.CreateBtn(aBtn: PGameIntfBtn; aGraf: TGrafres;
  yCord, xCord: Integer);
var
  str: string;
begin
  if aBtn^ = YesBtn then
    str := ResMan.lang[lsYes]
  else
    str := ResMan.lang[lsNo];
  aBtn^ := GamePix.AddTextBtn(yCord, xCord, str,
    ResMan.FontN[fiDlgBtn]);
  with aBtn^ do
  begin
    Imglist := aGraf.ImgLst;
    ImgObj.onMouse := Mouseevent;
    CurState := bsNormal;
  end;
  inc(felemCount);
end;

procedure TYesnoDlg.KeyPush(key: Word);
begin
  case key of
    vk_return:
      begin
        free;
        fcallback(drYes);
      end;
    vk_escape:
      begin
        free;
        fcallback(drno);
      end;
  end;
end;

procedure TYesnoDlg.Mouseevent(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; x, y: Integer);
var
  b: Boolean;
begin
  b := Sender = YesBtn.ImgObj;
  Free;
  if b then
    fcallback(drYes)
  else
    fcallback(drNo);
end;

//------------------------------------------------------------------------------

{ TTextDlg }

function TTextDlg.buildDlg(DlgText: string; acallback: TDlgcallbackproc):
  TsimpleDlg;
begin
  Fdlgtext := DlgText;
  CreateBkg(ResMan.GraphicN[giDlgBkg].Bmp);
  CreateText(DlgText);
  Result := Self;
end;

procedure TTextDlg.CreateText(DlgText: string);
begin
  flist := GamePix.AddLst(fBkg.yCord, fBkg.xCord);
  with flist do
  begin
    FontFace := 'tahoma';
    FontSize := 10;
    FontStyle := [fsbold];
    FontCol := clBlue;
    borderofs := 25;
    gipWidth := fBkg.gipWidth;
    inc(felemCount, AddMultiLineText(DlgText) + 1); // Add list and all Items
  end;
end;

//------------------------------------------------------------------------------

{ TOkBtnDlg }

function TOkBtnDlg.buildDlg(DlgText: string; acallback: TDlgcallbackproc):
  TsimpleDlg;
begin
  Fdlgtext := DlgText;
  CreateBkg(ResMan.GraphicN[giDlgBkg].Bmp);
  CreateText(DlgText);
  CreateBtn(@fokBtn, ResMan.GraphicN[giOkBtn], fBkg.yCord
    + (fBkg.gipHeight - 20) div 2, fBkg.xCord + (fBkg.gipWidth - 60) div 2);
  Result := Self;
end;

procedure TOkBtnDlg.CreateBtn(aBtn: PGameIntfBtn; aGraf: TGrafres;
  yCord, xCord: Integer);
begin
  aBtn^ := GamePix.AddBtn(yCord, xCord, aGraf);
  aBtn^.ImgObj.onMouse := Mouseevent;
  inc(felemCount);
end;

procedure TOkBtnDlg.KeyPush(key: Word);
begin
  inherited;
  case key of
    vk_return, vk_escape, vk_space:
      free;
  end;
end;

procedure TOkBtnDlg.Mouseevent(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; x, y: Integer);
begin
  free;
end;

//------------------------------------------------------------------------------

{ TGameIntfTextBtn }

procedure TGameIntfTextBtn.AfterConstruction;
begin
  FFont := TFont.Create;
end;

procedure TGameIntfTextBtn.BeforeDestruction;
begin
  FreeAndNil(FFont);
end;

procedure TGameIntfTextBtn.postprocess;
var
  wd, hg, Shift: Integer;
begin
  inherited;
  with (ImgObj as TGameImgIntf).Graphic as TBmp do
  begin
    Canvas.Font.Assign(Font);
    wd := Canvas.TextWidth(caption);
    hg := Canvas.TextHeight(caption);
    Canvas.Brush.Style := bsClear;
    case CurState of
      bshover..bsRightpush:
        Shift := fTextShift;
    else
      Shift := 0;
    end;
    Canvas.TextOut((Width - wd) div 2, (Height - hg) div 2 + Shift, caption);
  end;
end;

procedure TGameIntfTextBtn.Setcaption(const Value: string);
begin
  if Fcaption = Value then
    Exit;
  Fcaption := Value;
  ImgObj.IsChanged := True;
end;

procedure TGameIntfTextBtn.SetFont(const Value: TFont);
begin
  FFont.Assign(Value);
  ImgObj.IsChanged := True;
end;

procedure TGameIntfTextBtn.SetTextShift(const Value: Integer);
begin
  FTextShift := Value;
end;

//------------------------------------------------------------------------------

{ TGameImgmap }

procedure TGameImgmap.SetBufRect();
begin
  fbufRect.Top := Max(0, ViewZone.Top - CellIdent);
  fbufRect.Bottom := min(Ownr.Height - 1, ViewZone.Bottom + CellIdent);
  fbufRect.Left := Max(0, ViewZone.Left - CellIdent);
  fbufRect.Right := min(Ownr.Width - 1, ViewZone.Right + CellIdent);
end;

constructor TGameImgmap.Create(Collection: TCollection);
begin
  inherited;
  FMapBuffer := TBmp.Create;
  FMapDrawBuf := TBmp.Create;
  fBmp.Transparent := False;
  ScrollIdent := 10; // size of scrolling zone on edges of map
  CellIdent := 2; // Cells actually around Visible mapCells for mapbuffer
  // later may be transfered to TField
end;

destructor TGameImgmap.Destroy;
begin
  FreeAndNil(FMapBuffer);
  FreeAndNil(FMapDrawBuf);
  inherited;
end;

procedure TGameImgmap.DrawCell(ay, ax: TCellInt);
begin
  Ownr.GetCellGrafic(fbmp, ay, ax);
  FMapBuffer.Canvas.Draw((ax - BufRect.Left) * CellSize, (ay - BufRect.Top) *
    CellSize, fBmp);
end;

function TGameImgmap.GetGraphic: TGraphic;
var
  ax, ay: Word;
begin
  if IsChanged then
  begin
    ax := Ownr.LeftCol - BufRect.Left;
    ay := Ownr.TopRow - BufRect.Top;
    ax := ax * CellSize + XShift;
    ay := ay * CellSize + YShift;
    //    FMapDrawBuf.Canvas.CopyRect(Rect(0, 0, gioWidth, gioHeight),
    //      FMapBuffer.canvas, Rect(ax, ay, ax + gioWidth, ay + gioHeight));
    CopyRect(FMapDrawBuf.Canvas, FMapBuffer.canvas, Rect(0, 0, gioWidth,
      gioHeight), Rect(ax, ay, ax + gioWidth, ay + gioHeight));
  end;
  Result := FMapDrawBuf;
end;

{function TGameImgmap.GetOwnr: TGameIntfPart;
begin
  Result := FOwnr;
end;
 }

function TGameImgmap.GetViewZone: TRect;
begin
  Result := Ownr.ViewZone;
end;

procedure TGameImgmap.MouseDown(Shift: tShiftState);
begin
  inherited;

end;

procedure TGameImgmap.Mouseenter(Shift: tShiftState);
begin
  inherited;

end;

procedure TGameImgmap.Mouseleave(Shift: tShiftState);
begin
  inherited;
  IsChanged := False;
end;

procedure TGameImgmap.MouseMove(ay, ax: Word; Shift: tShiftState);
var
  aCol, aRow: TCellInt;
begin
  inherited;
  if not ActDisabled then
    PointToCell(ClientY(ay), ClientX(ax), aRow, aCol);
  {if aCol = (Ownr as TField).LeftCol then
    scroll(drLeft);
  if aCol = (Ownr as TField).LeftCol + (Ownr as TField).VisCol then
    scroll(drRight);
  if aRow = (Ownr as TField).TopRow then
    scroll(drUp);
  if aRow = (Ownr as TField).TopRow + (Ownr as TField).VisRow then
    scroll(drDown);}
  {if ClientX(ax) in [0..ScrollIdent] then
    scroll(drLeft);
  if gioWidth - ClientX(ax) <= ScrollIdent then
    scroll(drRight);
  if ClientY(ay) in [0..ScrollIdent] then
    scroll(drUp);
  if gioHeight - ClientY(ay) <= ScrollIdent then
    scroll(drDown);//}
end;

procedure TGameImgmap.MouseUp(Shift: tShiftState);
begin
  inherited;

end;

procedure TGameImgmap.PointToCell(ay, ax: Word; var aRow, aCol: TCellint);
begin // takes client coordinates
  aCol := Ownr.LeftCol + (ax + xShift) div CellSize;
  aRow := Ownr.TopRow + (ay + yShift) div CellSize;
  Ownr.CurCoords := Point(acol, arow);
end;

procedure TGameImgmap.RemapBuf(adir: NDiRection);
var
  ay, ax: Shortint;
  i: TCellInt;
  cnv: TCanvas;
  rec: tRect;
begin
  Desttocoorddelta(adir, ay, ax);
  cnv := FMapBuffer.Canvas;
  rec := cnv.ClipRect;
  offsetRect(rec, -ax * CellSize, -ay * CellSize);
  //cnv.CopyRect(rec, cnv, cnv.ClipRect);
  CopyRect(cnv, cnv, rec, cnv.ClipRect);
  case adir of
    drUp:
      for i := BufRect.Left to BufRect.Right do
        DrawCell(BufRect.Top, i);
    drDown:
      for i := BufRect.Left to BufRect.Right do
        DrawCell(BufRect.Bottom - 1, i);
    drLeft:
      for i := BufRect.Top to BufRect.Bottom do
        DrawCell(i, BufRect.Left);
    drRight:
      for i := BufRect.Top to BufRect.Bottom do
        DrawCell(i, BufRect.Right - 1);
  end;
  //  GetLocaleFormatSettings(1049, fs);
  //  fs.TimeSeparator := '.';
  //  FMapBuffer.SaveToFile(TimeToStr(now, fs) + '.bmp');
end;

procedure TGameImgmap.scroll(adir: NDiRection);
  function doshift(var Val: SmallInt; advance: Smallint): Boolean;
  begin
    val := Val + advance; // change the shift
    Result := not (val in [0..cellsize - 1]); // save that we need scroll
    Val := (Val + CellSize) mod CellSize; // fit shift value into 0..cellsize-1
    if Result and Ownr.scroll(adir) then // if viewzone changed
    begin
      SetBufRect; // reset bufrect according to viewzone
      RemapBuf(adir);
    end;
    IsChanged := True;
    Gamepix.PrepareBkg;
  end;
begin
  case adir of // ckecks whether shift needed, don't need it in edges of map
    drUp:
      if (fYShift - ScrollSpeed >= 0) or (ViewZone.Top <> 0) then
        doshift(fYShift, -ScrollSpeed);
    drDown:
      if (ViewZone.Bottom < Ownr.Height - 1)
        or ((gioHeight + YShift) mod cellsize <> 0) then
        doshift(fYShift, ScrollSpeed);
    drLeft:
      if (fxShift - ScrollSpeed >= 0) or (ViewZone.left <> 0) then
        doshift(fXShift, -ScrollSpeed);
    drRight:
      if (ViewZone.Right < Ownr.Width - 1)
        or ((gioWidth + XShift) mod cellsize <> 0) then
        doshift(fXShift, ScrollSpeed);
  end;
end;

procedure TGameImgmap.SetgioHeight(const Value: Word);
begin
  inherited;
  FMapBuffer.Height := CellSize * (Value div CellSize + 2 * CellIdent);
  FMapDrawBuf.Height := Value;
  FMask.Reset(Value, gioWidth, True);
end;

procedure TGameImgmap.SetgioWidth(const Value: Word);
begin
  inherited;
  FMapBuffer.Width := CellSize * (Value div CellSize + 2 * CellIdent);
  FMapDrawBuf.Width := Value;
  FMask.Reset(gioHeight, Value, True);
end;

procedure TGameImgmap.SetView(ay, ax: TCellInt);
var
  i, j: TCellInt;
begin
  SetBufRect;
  for i := BufRect.Top to BufRect.Bottom do
    for j := BufRect.Left to BufRect.Right do
      DrawCell(i, j);
  GamePix.PrepareBkg;
end;

function TGameImgmap.GetOwnr: TGameIntfField;
begin
  Result := inherited GetOwnr as TGameIntfField
end;

procedure TGameImgmap.SetOwnr(const Value: TGameIntfField);
begin
  inherited SetOwnr(Value);
end;

procedure TGameImgmap.Tick;
var
  aCol, aRow: TCellInt;
  ax, ay: Word;
  pnt: TPoint;
begin
  inherited Tick;
  if ActDisabled then
    Exit;
  pnt := GamePix.FCursorPoint;
  if Gamepix.Pixels[pnt.y, pnt.x] <> Self then
    Exit;
  ax := pnt.x;
  ay := Pnt.y;
  PointToCell(ClientY(ay), ClientX(ax), aRow, aCol);
  if ClientX(ax) in [0..ScrollIdent] then
    scroll(drLeft);
  if gioWidth - ClientX(ax) in [0..ScrollIdent] then
    scroll(drRight);
  if ClientY(ay) in [0..ScrollIdent] then
    scroll(drUp);
  if gioHeight - ClientY(ay) in [0..ScrollIdent] then
    scroll(drDown); //}
end;

{ TGameIntfField }

procedure TGameIntfField.SetCurCoords(const Value: TPoint);
begin
  if (Value.x in [0..Width - 1]) and (Value.y in [0..Height - 1]) then
    FCurCoords := Value;
end;

function TGameIntfField.GetViewZone: TRect;
begin
  Result.Top := TopRow;
  Result.Left := LeftCol;
  Result.Right := VisCol + LeftCol - 1;
  Result.Bottom := VisRow + TopRow - 1;
end;

procedure TGameIntfField.SetViewZone(const Value: tRect);
begin
  FTopRow := Value.Top;
  FLeftCol := Value.Left;
  FVisCol := Value.Right - Value.Left;
  FVisRow := Value.Bottom - Value.Top;
end;

procedure TGameIntfField.SetgipHeight(const Value: Word);
begin
  inherited;
  VisRow := Value div Cellsize;
end;

procedure TGameIntfField.SetgipWidth(const Value: Word);
begin
  inherited;
  VisCol := Value div Cellsize;
end;

function TGameIntfField.GetImgObj: tGameImgMap;
begin
  Result := fImgObj as tGameImgMap;
end;

procedure TGameIntfField.SetImgObj(const Value: tGameImgMap);
begin
  inherited SetImgObj(Value);
end;

function TGameIntfField.Scroll(aDir: NdiRection): Boolean;
var
  i: Integer;
begin
  i := toprow + leftcol;
  case aDir of
    drUp: TopRow := Max(0, TopRow - 1);
    drDown: if TopRow + VisRow < Height then
        TopRow := TopRow + 1;
    drLeft: LeftCol := Max(0, LeftCol - 1);
    drRight: if LeftCol + VisCol < Width then
        LeftCol := LeftCol + 1;
  end;
  Result := i <> toprow + leftcol;
  ImgObj.IsChanged := Result;
  if Result then
  begin
    ImgObj.SetBufRect; // reset bufrect according to viewzone
    ImgObj.RemapBuf(adir);
    ImgObj.Gamepix.PrepareBkg;
  end;
end;

procedure TGameIntfField.SetLeftCol(const Value: TCellInt);
begin
  FLeftCol := Value;
end;

procedure TGameIntfField.SetTopRow(const Value: TCellInt);
begin
  FTopRow := Value;
end;

procedure TGameIntfField.SetVisCol(const Value: TCellInt);
begin
  FVisCol := Value;
end;

procedure TGameIntfField.SetVisRow(const Value: TCellInt);
begin
  FVisRow := Value;
end;

procedure TGameIntfField.SetView(ay, ax: TCellInt); // refactor it !
begin
  {FLeftCol := ax + (ViewZone.Right - ViewZone.Left) - FVisCol;
  if ViewZone.Right > Width then
    begin
    SetView(ax-1, ay);
    Exit;
    end;}
  LeftCol := ax;
  {FTopRow := ay + (ViewZone.Bottom - ViewZone.Top) - FVisRow;
  if ViewZone.Bottom > Height then
    begin
    SetView(ax, ay-1);
    Exit;
    end;}
  TopRow := ay;
  ImgObj.SetView(ax, ay);
end;

function TGameIntfField.GetVisCol: TCellInt;
begin
  {if ImgObj.xShift <> 0 then
    fVisCol := gipWidth div Cellsize + 1
  else fVisCol := gipWidth div Cellsize + 2;
  Result := fVisCol;}
  Result := FVisCol;
  if (ImgObj.XShift = 0) and (gipWidth mod Cellsize = 0) then
    Exit;
  if gipWidth + ImgObj.XShift - (Result + 1) * cellsize <= 0 then
    inc(Result)
  else
    inc(Result, 2);

end;

function TGameIntfField.GetVisRow: TCellInt;
begin
  Result := FVisRow;
  if (ImgObj.YShift = 0) and (gipHeight mod Cellsize = 0) then
    Exit;
  if gipHeight + ImgObj.YShift - (Result + 1) * cellsize <= 0 then
    inc(Result)
  else
    inc(Result, 2);

end;

{ TGameIntfBkgdSimple }

constructor TGameIntfBkgdSimple.Create(Collection: TCollection);
begin
  inherited;
  ImgObj := TGameImgBkgd.Create(Coll.GamePix.ImgObjColl);
  ImgObj.Ownr := Self;
end;

{ TGameIntfBkgdcommon }

function TGameIntfBkgdcommon.GetImgObj: TGameImgBkgd;
begin
  Result := inherited GetImgObj as TGameImgBkgd;
end;

procedure TGameIntfBkgdcommon.SetImgObj(const Value: TGameImgBkgd);
begin
  inherited SetImgObj(Value);
end;

{ TGameIntfPavedBkgd }

constructor TGameIntfPavedBkgd.Create(Collection: TCollection);
begin
  inherited;
  ImgObj := TGameImgPavedBkgd.Create(Coll.GamePix.ImgObjColl);
  ImgObj.Ownr := Self;
end;

function TGameIntfPavedBkgd.GetImgObj: TGameImgPavedBkgd;
begin
  Result := inherited GetImgObj as TGameImgPavedBkgd
end;

procedure TGameIntfPavedBkgd.SetImgObj(const Value: TGameImgPavedBkgd);
begin
  inherited SetImgObj(Value);
end;

{ TGameImgPavedBkgd }

function TGameImgPavedBkgd.GetGraphic: TGraphic;
var
  hg, wd, sw, sh: Word;
begin
  FBmp.Width := gioWidth;
  FBmp.Height := gioHeight;
  hg := 0;
  wd := 0;
  sw := fBmpin.Width;
  sh := fBmpin.Height;
  while (hg <= GioHeight) and (wd <= GioWidth) do
  begin
    FBmp.Canvas.Draw(wd, hg, fbmpin);
    if JobMode in [jmHorz, jmBoth] then
      inc(wd, sw);
    if JobMode in [jmVert, jmBoth] then
      inc(hg, sh);
  end;
  Result := FBmp;
end;

{ TGameImgBkgd }

constructor TGameImgBkgd.Create(Collection: TCollection);
begin
  inherited;
  //  FBmpOut := TBmp.Create;
end;

destructor TGameImgBkgd.Destroy;
begin
  //  FreeAndNil(FBmpOut);
  inherited;
end;

function TGameImgBkgd.GetGraphic: TGraphic;
begin
  Result := FBmpin;
end;

{ TGameIntfFlipedBkgd }

constructor TGameIntfFlipedBkgd.Create(Collection: TCollection);
begin
  inherited;
  ImgObj := TGameImgFlipedBkgd.Create(Coll.GamePix.ImgObjColl);
  ImgObj.Ownr := Self;
end;

function TGameIntfFlipedBkgd.GetImgObj: TGameImgFlipedBkgd;
begin
  Result := inherited GetImgObj as TGameImgFlipedBkgd;
end;

procedure TGameIntfFlipedBkgd.SetImgObj(const Value: TGameImgFlipedBkgd);
begin
  inherited SetImgObj(Value);
end;

{ TGameImgFlipedBkgd }

function TGameImgFlipedBkgd.GetGraphic: TGraphic;
begin
  FBmp.Assign(fbmpIn);
  if JobMode in [jmHorz, jmBoth] then
    Flip(FBmp, False, True);
  if JobMode in [jmVert, jmBoth] then
    Flip(FBmp, True, False);
  Result := FBmp;
end;

{ TGameImgStretchedBkgd }

function TGameImgStretchedBkgd.GetGraphic: TGraphic;
begin
  FBmp.Width := gioWidth;
  FBmp.Height := gioHeight;
  FBmp.Canvas.StretchDraw(Rect(0, 0, gioWidth, gioheight), fbmpIn);
  Result := FBmp;
  //FBmpOut.SaveToFile({TimeToStr(now, fs) + }'1.bmp')
end;

{ TGameIntfStretchedBkgd }

constructor TGameIntfStretchedBkgd.Create(Collection: TCollection);
begin
  inherited;
  ImgObj := TGameImgStretchedBkgd.Create(Coll.GamePix.ImgObjColl);
  ImgObj.Ownr := Self;
end;

function TGameIntfStretchedBkgd.GetImgObj: TGameImgStretchedBkgd;
begin
  Result := inherited GetImgObj as TGameImgStretchedBkgd;
end;

procedure TGameIntfStretchedBkgd.SetImgObj(
  const Value: TGameImgStretchedBkgd);
begin
  inherited SetImgObj(Value);
end;

{ TGameIntfDlgBkgd }

constructor TGameIntfDlgBkgd.Create(Collection: TCollection);
begin
  inherited;
  ImgObj := TGameImgBkgd.Create(Coll.GamePix.ImgObjColl);
  ImgObj.Ownr := Self;
end;

{ TGameImgAnim }

procedure TGameImgAnim.AfterConstruction;
begin
  inherited;
  TmpBmp := TBmp.Create;
end;

procedure TGameImgAnim.BeforeDestruction;
begin
  inherited;
  FreeAndNil(TmpBmp);
end;

function TGameImgAnim.GetgioHeight: Word;
begin
  Result := FImgLst.Height;
end;

function TGameImgAnim.GetgioWidth: Word;
begin
  Result := FImgLst.Width;
end;

function TGameImgAnim.GetGraphic: TGraphic;
begin
  fbmp.transparent := True;
  fbmp.transparentcolor := transcol;

  Result := fBmp;
end;

procedure TGameImgAnim.mouseup(Shift: tShiftState);
begin
  inherited;

end;

procedure TGameImgAnim.SetGraphic(const Value: TGraphic);
begin
  inherited;

end;

procedure TGameImgAnim.Tick;
begin
  IsChanged := True;
  if ActDisabled then
    Exit;
  inherited;

  //  Fcadre := (Fcadre + 1) mod FImgLst.Count;
  fBmp.Width := 0;
  Fcadre := (Fcadre + 1) mod FImgLst.Count;
  //Imglst.BlendColor := transCol;
  Imglst.getbitmap(Fcadre, fBmp);
  //SetBmp(fBmp, transcol);
  //  TmpBmp.Transparent := True;
  //  TmpBmp.TransparentColor := Imglst.BlendColor;

    //
    //fBmp.Assign(TmpBmp);

end;

{ TGameIntfAnim }

constructor TGameIntfAnim.Create(Collection: TCollection);
begin
  inherited;
  FImgObj := TGameImgAnim.Create(Coll.GamePix.ImgObjColl);
  ImgObj.Ownr := Self;
end;

function TGameIntfAnim.GetImgObj: tGameImgAnim;
begin
  Result := FimgObj as tGameImgAnim;
end;

procedure TGameIntfAnim.GrafDefinesSize;
begin
  gipWidth := ImgObj.gioWidth;
  gipHeight := ImgObj.gioHeight;
end;

procedure TGameIntfAnim.SetImgObj(const Value: tGameImgAnim);
begin
  inherited SetImgObj(Value);
end;

end.

