unit checkMem;

interface

implementation

uses sysUtils, classes;

const
  fname = 'lostmem.txt';

var
  HPs, HPe: THeapStatus;
  Lost: integer;
  sl : TStringList;
initialization
  HPs := getHeapStatus;
finalization
  HPe := getHeapStatus;
  Lost := HPe.TotalAllocated - HPs.TotalAllocated;
  DeleteFile(fname);
  if Lost > 0 then
  begin
    sl := TStringList.Create;
    sl.Add(Format('lostMem: %d', [Lost]));
    sl.SaveToFile(fname);
    sl.Free;
  end;
end.

