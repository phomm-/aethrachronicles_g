unit Ucommon;

interface

uses SynGDIPlus, Windows, Graphics, Forms, Contnrs, Classes;

type

  TMyObjList = class(TObjectList)
    procedure Clear(); override;
  end;

  IKeyUsable = interface(IInterface)
    procedure KeyPress(Key: Word; Shift: TShiftState);
    procedure RegKey(Key: Word; Shift: TShiftState; Event: TNotifyEvent);
  end;

  Tbmp = TBitmap;

  NFormState = (fsMenu, fsMap, fsOptions, fsGame);
  // todo options_Form and other

// define all the enums
  Nlngstr = (
    lsYes,
    lsNo,
    lsleavegame,
    lsnewgame,
    lsnomaps,
    lsCancel,
    lsLeavetoMenu);

  NFontid = (
    fiDlgbtn,
    fiDlgText,
    fiTitle,
    fiUsual);

  NGrafId = (
    giDefbkg,
    giDlgbkg,
    giEmptybtn,
    giOkbtn,
    giNewGame,
    giExit,
    giResume,
    giInfo,
    giStart,
    giTerDirt,
    giTersand,
    giTerGrass,
    giTersnow,
    giTerswamp,
    giTerrough,
    giTersubter,
    giTerlava,
    giTerwater,
    giTerrock,
    giMenuform,
    giMapform,
    giCorner,
    giSide,
    giGuipane,
    giAngel,
    giLoadScr
    );

  NGraftype = (
    gtNone,
    gtBmp,
    gtPng,
    gtDef,
    gtImglst,
    gtBmpSet,
    gtPngSet);

  NDiRection = (drUp, drRightUp, drRight, drRightDown, drDown, drLeftDown,
    drLeft, dLeftUp);
  TCellInt = type Word;
  Ntertype = (ttDirt, ttsand, ttGrass, ttsnow, ttswamp, ttrough, ttcave, ttlava,
    ttwater, ttvoid);

function Max(arg1, arg2: integer): integer;
function Min(arg1, arg2: integer): integer;
procedure LoadPngtoBmp(aBmp: TBitmap; fName: string);
procedure LoadCursors();
procedure Desttocoorddelta(aDir: NDiRection; var ay, ax: shortint);

const // cursors
  crUsual = 5;
  crCheck = 6;

const
  AppVer = '0.1.5';
  AppTitle = 'AETHRA Chronicles Remake';
  CellSize = 32;
  ScrollSpeed = 16;
  TmrInterval = 50;

var
  AppPath,
    GrafPath,
    cfgPath,
    DefsPath: string;
  terassoc: array[Low(Ntertype)..High(Ntertype)] of NGrafid =
    (giTerDirt,
    giTerSand,
    giTerGrass,
    giTerSnow,
    giTerSwamp,
    giTerRough,
    giTerSubter,
    giTerLava,
    giTerWater,
    giTerRock);

resourcestring
  langIni = 'lang.Ini';
  GrafIni = 'Graf.Ini';
  FontIni = 'Font.Ini';
  OptionsIni = 'acr.Ini';

  strnoFile = 'File not exists %s';
  strnoImglst = 'Not created imagelist for def ';
  strnofunction = 'No function assigned to call';
  strwrongBmp = 'Not valid Bmp used for Mask';
  strBadResManarg = 'Wrong ResMan argument';
  stremptybuf = 'Empty main buffer';
  strBadFont = 'Wrong Font Loading';
  strRedrawError = 'Redrawing error';
  SBadMoverArg = 'Wrong Mover argument';
  //------------------------------------------------------------------------------

implementation

procedure LoadCursors();
begin
  Screen.Cursors[crusual] := LoadCursorFromFile(PAnsiChar(GrafPath +
    'usual.ani'));

end;

function max(arg1, arg2: integer): integer;
begin
  if arg1 > arg2 then
    result := arg1
  else
    result := arg2;
end;

//------------------------------------------------------------------------------

function min(arg1, arg2: integer): integer;
begin
  if arg1 < arg2 then
    result := arg1
  else
    result := arg2;
end;

//------------------------------------------------------------------------------

procedure Desttocoorddelta(aDir: NDiRection; var ay, ax: shortint);
begin
  ax := round(sin(Ord(aDir) * pi / 4));
  ay := -round(cos(Ord(aDir) * pi / 4));
end;

procedure LoadPngtoBmp(aBmp: TBitmap; fName: string);
var
  apng: TPngImage;
  lbmp: TBitmap;
begin
  apng := TPngImage.Create;
  apng.LoadFromFile(fName);
  {aBmp.PixelFormat := pf24bit;
  aBmp.Width := apng.Width;
  aBmp.Height := apng.Height;}
  lBmp := apng.ToBitmap; // do not use assign ! because tobitmap creates bitmaps
  aBmp.Assign(lbmp);
  lbmp.free; // must be freed
  apng.free;
end;

{ TMyObjList }

procedure TMyObjList.Clear;
var
  i : Integer;
begin
  for i := Count - 1 downto 0 do
    Delete(i);
  inherited;  
end;

end.

