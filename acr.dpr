program acr;

uses
  checkMem in 'units\checkMem.pas',
  Forms,
  Umainform in 'Umainform.pas' {Form1},
  Ugamesys in 'Ugamesys.pas',
  Ucommon in 'Ucommon.pas',
  Upixeng in 'Upixeng.pas',
  Ugraph in 'Ugraph.pas',
  Umapmgr in 'Umapmgr.pas',
  Uresman in 'Uresman.pas',
  UMover in 'UMover.pas',
  uSound in 'USound.pas',
  Bass in 'units\Bass.pas';

{$R *.res}

begin
  {$IFDEF DEBUG}
    {$IF COMPILERVERSION >= 18}
    ReportMemoryLeaksOnShutdown := True;
    {$IFEND}
  {$ENDIF}
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
