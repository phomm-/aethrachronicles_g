unit UResMan;

interface

uses
  SysUtils, IniFiles, TypInfo, Windows, Graphics, Controls, Classes,
  // other units
  Usingleton,
  // Self-made units
  UGraph, Ucommon;

type


  RGrafcfg = reCord  // for config Loading
    Width,
    Height,
    Cadres : Word;
    transCol : Cardinal;
    BoundRect : TRect;
    Center : TPoint;
  end;
         // classes for resources
  TGrafRes = class(TObject)
  private
    fImgLst: Timagelist;
    fBmp: TBmp;
    fGraftype: NGraftype;
    FextGrafid : NGrafId;
    procedure SetextGrafId(const Value: NGrafId);
  protected
    procedure SetBmp(const Value: TBmp);
    function GetBmp: TBmp;
    procedure SetImgLst(const Value: Timagelist);
    function GetImgLst: Timagelist;
  public
    Destructor Destroy(); override;
    procedure Init(aGraftype: NGraftype; fName: TFileName; acfg : RGrafcfg);
    property Bmp: TBmp read GetBmp write SetBmp;
    property ImgLst: Timagelist read GetImgLst write SetImgLst;
    property ExtGrafId : NGrafId read FExtGrafId write SetextGrafId;
  end;

  TFontRes = class(TObject)
  private
    fFontMem: HFont;
    fFont: tFont;
    FexternalFont : Boolean;
    procedure SetFont(const Value: tFont);
  protected

  public
    constructor create();
    Destructor Destroy(); override;
    procedure LoadFromFile(fName: TFileName);
    property Font: tFont read FFont write SetFont;
  end;

  TLangar = array[Low(nlngstr)..High(nlngstr)] of string;

  TLangRes = class(TSingleton)
  private
    flangar: Tlangar;
    function Getlang(index: Nlngstr): string;
  public
    procedure Loadlang(fName: TFileName);
    procedure Getdeflang;
    property lang[index: Nlngstr]: string read Getlang; default;
  end;

  // ResMan holds all Graphics giving it by NGrafid(as index, or some const)
  // the given type of Graphics determines the Object

  TResMan = class(TSingleton)
  private
    FLang: TLangRes;
    FGrafLst: TList;
    FFontLst: TList;
    function GetFontN(index: nFontid): TFont;
    function GetFontI(index: integer): TFont;
  protected
    constructor create; override;
    function GetGraphicI(index: integer): TGrafRes;
    function GetGraphicN(index: NGrafId): TGrafRes;
    procedure FillGraf(aval: integer);
    procedure FillFont(aval: integer);
  public
    Destructor Destroy; override;
    procedure LoadGraphics(fName: TFileName);
    procedure LoadFonts(fName: TFileName);
    property GraphicI[index: integer]: TGrafRes read GetGraphicI;
    property GraphicN[index: nGrafid]: TGrafRes read GetGraphicN;
    property FontN[index: nFontid]: TFont read GetFontN;
    property FontI[index: integer]: TFont read GetFontI;
    property lang: TLangres read flang;
  end;

const
  lastGrafid = Ord(high(NGrafid)); // must be used for defIning other constants
  lastFontid = Ord(high(NFontid)); // by a number, not an ID

  //------------------------------------------------------------------------------
function ResMan: TResMan;   // some functions to hide actual implementation
procedure createResMan();
procedure DestroyResMan();

//------------------------------------------------------------------------------

implementation

const
  deftransCol = clWhite;

var
  fResMan: TResMan;

function ResMan: TResMan;
begin
  Result := fResMan;
end;

procedure createResMan();
begin
  fResMan := tResMan.Getinstance
end;

procedure DestroyResMan();
begin
  FreeAndNil(fResMan);
end;
          // config strings
const
  cfglngmain     = 'lngmain';
  cfgcurlang     = 'curlang';
  cfgGrafmain    = 'Grafmain';
  cfgFileName    = 'FileName';
  cfggrtype      = 'type';
  cfgFontmain    = 'Fontmain';
  cfgFontface    = 'face';
  cfgFontsize    = 'size';
  cfgFontColor   = 'Color';
  cfgWidth       = 'Width';
  cfgHeight      = 'Height';
  cfgCadres      = 'Cadres';
  cfgtransCol    = 'transCol';
  cfgGrafid      = 'Grafid';
  cfgBoundLeft   = 'BoundLeft';
  cfgBoundRight  = 'BoundRight';
  cfgBoundTop    = 'BoundTop';
  cfgBoundBottom = 'BoundBottom';
  cfgHorzCenter  = 'HorzCenter';
  cfgVertCenter  = 'VertCenter';

  Lng_def: TLangar = (
    'Yes',
    'No',
    'You really wish to leave game ?',
    'You really wish to start a new game ?',
    'No maps available!',
    'Cancel',
    'Leave to Main Menu? Te game will be lost');


// taken From http://forum.vinGrad.ru/forum/Topic-22223/anchor-entry1604536/0.html
function GetFontName (FontFileA : PChar) : String;
type
  TGetFontResourceInfoW = function (FontPath : PWideChar; var BufSize : DWORD;
    FontName : PWideChar; dwFlags : DWORD) : DWORD; stdcall;
var
  GetFontResourceInfoW : TGetFontResourceInfoW;
  FontFileW : PWideChar;
  FontNameW : PWideChar;
  FontFileWSize, FontNameSize : DWORD;
begin
  Result := '';
  GetFontResourceInfoW := GetProcAddress(GetModuleHandle('gdi32.dll'), 'GetFontResourceInfoW');
  if @GetFontResourceInfoW = nil then Exit;
  if AddFontResource(FontFileA) = 0 then Exit;

  FontFileWSize := (Length(FontFileA)+1)*2;
  GetMem(FontFileW, FontFileWSize);
  StringToWideChar(FontFileA, FontFileW, FontFileWSize);

  FontNameSize := 0;
  FontNameW := nil;
  GetFontResourceInfoW (FontFileW, FontNameSize, FontNameW, 1);
  GetMem (FontNameW, FontNameSize);
  FontNameW^ := #0; // �� ������ ������-������ ������
  GetFontResourceInfoW (FontFileW, FontNameSize, FontNameW, 1);

  Result := FontNameW;
  FreeMem (FontFileW);
  FreeMem (FontNameW);

  ReMoveFontResource(FontFileA);
end;


  { TLang }

procedure Tlangres.Getdeflang;  // Loads default lang From constant array
var
  i: Nlngstr;
begin
  for i := Low(Nlngstr) to High(Nlngstr) do
    flangar[i] := lng_def[i];
end;

function Tlangres.Getlang(index: Nlngstr): string;
begin
  Result := flangar[index];
end;

procedure TLangres.Loadlang(fName: TFileName);
var
  Inif: tmemIniFile;
  str, sect: string;
  i: Nlngstr;
begin
  Inif := tmemIniFile.create(fName);
  try
    try
      Assert(Inif <> nil, Format(strnoFile, [fName]));
      Assert(Inif.SectionExists(cfglngmain), strBadResManarg);
      sect := Inif.Readstring(cfglngmain, cfgcurlang, '');
      Assert((sect <> '') and (Inif.SectionExists(sect)), strBadResManarg);
      for i := low(Nlngstr) to high(Nlngstr) do
      begin
        str := GetEnumName(typeinfo(Nlngstr), ord(i));
        flangar[i] := Inif.Readstring(sect, str, lng_def[i]);
      end;
    except
      Getdeflang;
    end;
  finally
    FreeAndNil(Inif);
  end;
end;

{ TResMan }

constructor TResMan.create;
begin
  inherited;
  FGrafLst := tlist.create;
  fillGraf(lastGrafid + 1);
  FFontLst := tlist.create;
  fillFont(lastFontid + 1);
  flang := TLangRes.GetInstance;
end;

Destructor TResMan.Destroy;
var
  i: Integer;
begin
  i := 0;
  while i < FGrafLst.Count do
  begin
    TGrafRes(FGrafLst.Items[i]).Free;
    Inc(i);
  end;
  i := 0;
  while i < FFontLst.Count do
  begin
    TFontRes(FFontLst.Items[i]).Free;
    Inc(i);
  end;
  FreeAndNil(FGrafLst);
  FreeAndNil(FFontLst);
  FreeAndNil(flang);
  inherited;
end;

procedure TResMan.LoadGraphics(fName: TFileName);
var
  Inif: tmemIniFile;
  sl: tstringlist;
  str: string;
  gt: NGraftype;
  i, idval, val: integer;
  acfg : RGrafcfg;
begin
  Inif := tmemIniFile.create(fName);
  try
    sl := TStringList.Create;
    Assert(Inif <> nil, Format(strnoFile, [fName]));
    Assert(Inif.SectionExists(cfgGrafmain), strBadResManarg);
    Inif.ReadSection(cfgGrafmain, sl);  // Load all IDs to list
    i := 0;
    while i < sl.Count do  // for each ID Load the config
    begin
      if not TryStrToInt(sl[i], idval) then  // if is numeric ID
        idval := Ord(NGrafid(GetEnumValue(typeinfo(NGrafid), sl[i]))); // enum ID
      fillGraf(idval);
      str := Inif.ReadString(sl[i], cfgGrafid, '');
      val := GetEnumValue(typeinfo(NGrafid), str);
      TGrafRes(FGrafLst[idval]).ExtGrafId := nGrafid(val);
      if val = -1 then
      begin
        str := Inif.Readstring(sl[i], cfggrtype, '');
        gt := NGraftype(GetEnumValue(typeinfo(NGraftype), str));
        Assert(Ord(gt) >= 0, strBadResManarg);
        str := Inif.ReadString(sl[i], cfgFileName, '');
        Assert(str <> '', strBadResManarg);
        acfg.Width := Inif.ReadInteger(sl[i], cfgWidth, 0);
        acfg.Height := Inif.ReadInteger(sl[i], cfgHeight, 0);
        acfg.Cadres := Inif.ReadInteger(sl[i], cfgCadres, 0);
        acfg.transCol := Inif.ReadInteger(sl[i], cfgtransCol, DefTransCol);
        {
        acfg.BoundRect.Left := Inif.ReadInteger(sl[i], cfgBoundLeft, 0);
        acfg.BoundRect.Right := Inif.ReadInteger(sl[i], cfgBoundRight, 0);
        acfg.BoundRect.Top := Inif.ReadInteger(sl[i], cfgBoundTop, 0);
        acfg.BoundRect.Bottom := Inif.ReadInteger(sl[i], cfgBoundBottom, 0);
        }
        acfg.Center.X := Inif.ReadInteger(sl[i], cfgHorzCenter, 0);
        acfg.Center.Y := Inif.ReadInteger(sl[i], cfgVertCenter, 0);
        TGrafRes(FGrafLst[idval]).Init(gt, GrafPath + str, acfg); // Load actual data
      end;
      Inc(i);
    end;
  finally
    FreeAndNil(Inif);
    FreeAndNil(sl);
  end;
end;

procedure TResMan.LoadFonts(fName: TFileName); // same scheme as LoadGraphics
var
  Inif: tmemIniFile;
  sl: tstringlist;
  fnt: TFont;
  str: string;
  i, idval: integer;
  val: Cardinal;
begin
  Inif := tmemIniFile.create(fName);
  try
    sl := TStringList.Create;
    Assert(Inif <> nil, Format(strnoFile, [fName]));
    Assert(Inif.SectionExists(cfgFontmain), strBadResManarg);
    Inif.ReadSection(cfgFontmain, sl);
    i := 0;
    while i < sl.Count do
    begin
      if TryStrToInt(sl[i], idval) then
      else
        idval := Ord(NFontid(GetEnumValue(typeinfo(NFontid), sl[i])));
      fillFont(idval);
      fnt := FontI[idval];
      str := Inif.Readstring(sl[i], cfgFontface, '');
      if str <> '' then
        fnt.Name := str;
      str := Inif.Readstring(sl[i], cfgFileName, '');
      if str <> '' then
        TFontRes(FFontLst[idval]).LoadFromFile(cfgPath + str);
      val := Inif.ReadInteger(sl[i], cfgFontsize, 0);
      Assert(val <> 0, strBadResManarg);
      fnt.size := val;
      val := Inif.ReadInteger(sl[i], cfgFontColor, 0);
      Assert(val <> 0, strBadResManarg);
      fnt.Color := val;
      Inc(i);
    end;
  finally
    FreeAndNil(Inif);
    FreeAndNil(sl);
  end;
end;

function TResMan.GetGraphicI(index: integer): TGrafRes;
var
  id : Integer;
begin
  id := ord(TGrafRes(FGrafLst[index]).FextGrafid);
  if id = 255 then
    Result := TGrafRes(FGrafLst[index])
  else Result := TGrafRes(FGrafLst[id]);
end;

function TResMan.GetGraphicN(index: nGrafid): TGrafRes;
begin
  Result := GetGraphicI(ord(index));
end;

procedure TResMan.FillGraf(aval: integer);
begin
  if FGrafLst.capacity >= aval then
    Exit;
  FGrafLst.capacity := aval;
  while FGrafLst.Count < aval do
    FGrafLst.Add(TGrafRes.create);
      // just fill the list for future use by LoadGrafics
end;

procedure TResMan.FillFont(aval: integer);
begin
  if FFontLst.capacity >= aval then
    Exit;
  FFontLst.capacity := aval;
  while FFontLst.Count < aval do
    FFontLst.Add(TFontRes.create);
      // just fill the list for future use by LoadGrafics
end;

function TResMan.GetFontN(index: nFontid): TFont;
begin
  Result := FontI[Ord(index)];
end;

function TResMan.GetFontI(index: integer): TFont;
begin
  Result := TFontRes(FFontLst[index]).Font;
end;

{ TGrafRes }

procedure TGrafRes.Init(aGraftype: NGraftype; fName: TFileName ; acfg : RGrafcfg);
var
  aPath, aName : string;
  aBmp : TBmp;
  i : integer;
begin
  if not (aGraftype in [gtBmpSet, gtPngSet]) then  // check File existence
    Assert(Fileexists(fName), Format(strnoFile, [fName]));
  fGraftype := aGraftype;
  case aGraftype of      // each Graftype is Loaded in special manner
    gtBmp, gtPng:        // Graftype of Bmp and ImgLst remains as is
      begin              // other's is changed to Bmp or ImgLst
        fGraftype := gtBmp;
        Bmp := TBmp.Create;
        LoadPngtoBmp(Bmp, fName);
        Bmp.Transparent := True;
        Bmp.TransparentColor := acfg.transCol;
      end;
    gtDef:
      begin
        fGraftype := gtImgLst;
        ImgLst := TImageList.Create(nil);
        DefToImgLst(fName, ImgLst); 
        ImgLst.BlendColor := acfg.transCol;
      end;
    gtImgLst:
      begin
        ImgLst := TImageList.Createsize(acfg.Width, acfg.Height);
        aBmp := TBmp.Create;
        aBmp.LoadFromFile(fName);
        ImgLst.AddMasked(aBmp, acfg.transCol);
        ImgLst.BlendColor := acfg.transCol;
        FreeAndNil(aBmp);
      end;
    gtBmpSet, gtPngSet:
      begin
        fGraftype := gtImgLst;
        Assert(acfg.Cadres > 0 , strBadResManarg);
        aPath := IncludeTrailingPathDelimiter(extractFiledir(fName));
        aName := extractFileName(fName);
        aBmp := TBmp.Create;
        LoadPngtoBmp(aBmp, aPath + '0' + aName);
        ImgLst := TImageList.Createsize(aBmp.Width, aBmp.Height);
        ImgLst.AddMasked(aBmp, acfg.transCol);
        i := 1;
        while i < acfg.Cadres do
        begin
          LoadPngtoBmp(aBmp, aPath + inttostr(i) + aName);
          ImgLst.AddMasked(aBmp, acfg.transCol);
          inc(i);
        end;
        ImgLst.BlendColor := acfg.transCol;
        FreeAndNil(aBmp);
      end;
  else
    raise EInvalidOp.Create(strBadResManarg);
  end;
end;

Destructor TGrafRes.Destroy;
begin
  if ord(FextGrafid) <> -1 then
  begin
    fBmp.Free;
    fImgLst.Free;
  end;
  inherited;
end;

function TGrafRes.GetBmp: TBmp;
begin
  Assert(fGraftype = gtBmp, strBadResManarg);
  Result := fBmp;
end;

function TGrafRes.GetImgLst: Timagelist;
begin
  Assert(fGraftype = gtImgLst, strBadResManarg);
  Result := fImgLst;
end;

procedure TGrafRes.SetBmp(const Value: TBmp);
begin
  Assert((Value is TBmp), strBadResManarg);
  FBmp := Value;
end;

procedure TGrafRes.SetImgLst(const Value: Timagelist);
begin
  Assert((Value is TImageList), strBadResManarg);
  FImgLst := Value;
end;

procedure TGrafRes.SetextGrafId(const Value: NGrafId);
begin
  FExtGrafId := Value;
end;

{ TFontRes }

constructor TFontRes.create();
begin
  inherited;
  fFont := TFont.Create;
end;

Destructor TFontRes.Destroy;
begin
  if FexternalFont then
    ReMoveFontMemResourceEx(fFontMem);
  FreeAndNil(fFont);
  inherited;
end;

procedure TFontRes.LoadFromFile(fName: TFileName);
var
  ms: TMemoryStream;
  i: DWord;
  fhFont : THandle;
  fntface : string;
begin
  ms := TMemoryStream.Create;
  try
    ms.LoadFromFile(fName);
    fFontMem := AddFontMemResourceEx(ms.Memory, ms.Size, nil, @i);
    fntface := GetFontName(PAnsiChar(fName));
    fHFont := CreateFontA(36, 0, 0, 0, 0, 0, 0, 0, DEFAULT_CHARSet,
      OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
      DEFAULT_PITCH, PAnsiChar(fntface));
    fFont.Handle :=  fhFont;
    FexternalFont := True;
  finally
    FreeAndNil(ms);
  end;
end;

procedure TFontRes.SetFont(const Value: tFont);
begin
  Assert(Value <> nil, strBadFont);
  FFont.Assign(Value);
end;

end.

