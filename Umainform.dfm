object MainForm: TMainForm
  Left = 371
  Top = 105
  Width = 798
  Height = 570
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnMouseDown = FormMouseDown
  OnMouseMove = FormMouseMove
  OnMouseUp = FormMouseUp
  OnPaint = FormPaint
  OnResize = FormResize
  DesignSize = (
    790
    543)
  PixelsPerInch = 96
  TextHeight = 13
  object SgMap: TStringGrid
    Left = 80
    Top = 88
    Width = 441
    Height = 377
    Anchors = [akLeft, akTop, akRight, akBottom]
    FixedCols = 0
    FixedRows = 0
    GridLineWidth = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goThumbTracking]
    TabOrder = 0
    OnDrawCell = SgMapDrawCell
    OnMouseUp = SgMapMouseUp
    OnTopLeftChanged = SgMapTopLeftChanged
  end
end
